$(document).ready(function(){
    $('.pagination li').addClass('page-item');
    $('.pagination li a').addClass('page-link');
    $('.pagination span').addClass('page-link');


    if ($(window).width() < 550) {  
 
            $('.redes').removeClass('list-group').addClass('list-inline');
            $('.redes li').removeClass('list-group-item').addClass('list-inline-item');
 
        }   else{
            $('.redes').addClass('list-group').removeClass('list-inline');
            $('.redes li').addClass('list-group-item').removeClass('list-inline-item');
        }

    $(window).resize(function(){

        if ($(window).width() < 550) {  
 
            $('.redes').removeClass('list-group').addClass('list-inline');
            $('.redes li').removeClass('list-group-item').addClass('list-inline-item');
 
        }  else{
            $('.redes').addClass('list-group').removeClass('list-inline');
            $('.redes li').addClass('list-group-item').removeClass('list-inline-item');
        }   
 
 });
});
