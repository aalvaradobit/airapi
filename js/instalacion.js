$(document).ready(function(){
$('#btnLista1').click(function(e){
    e.preventDefault();
    $.ajaxSetup({

    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
    $.ajax({
        "_token": $('#token').val(),
        url: "{{ url('/instalacion') }}",
        method: 'post',
        data: {
            idInstalacion: $('.idInstalacion').val(),
            carrouselTxt: $('#txtLista1').val(),
        },
        success: function(result){
            
            var car = "<li class='list-group-item'>" + $('#txtLista1').val() + "</li>";
          
            $('#lista2').append(car);
            $('#txtLista1').val('');
        }});
    });



$('.btnEliminarText').click(function(e){
    e.preventDefault();

    var id = $('.btnEliminarText').attr('id');
    console.log(id);
    $.ajaxSetup({

    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
    $.ajax({
        
        url: "{{ url('/instalacion')}}"+'/'+id,
        method: 'get',

        success: function(result){
           console.log(result);
           
            $('#txt1-'+id).remove();
        }});
    });
});

</script>
