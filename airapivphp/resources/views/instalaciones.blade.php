@extends('..layouts.app')

@section('title', 'Airapi Memorial Park, salas de velación y Capilla en Querétaro')
@section('description', 'En Memorial Park contamos con 4 salas de velación de lujo, crematorio de última generación, capilla, cafetería y 2 suites privadas en Querétaro.')
@section('keywords', 'airapi memorial park, velación en querétaro, crematorio en querétaro')


@section('content')

<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
  <ol class="carousel-indicators">
    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="3"></li>
  </ol>
  <div class="carousel-inner">
  
    <div class="carousel-item active">
      <img class="d-block w-100" src="{{ asset('img/instalaciones/instalaciones_mainbanner_01.jpg') }}" alt="First slide">
      <div class="bannerText col-md-5">
      <img class="pl-5" src="img/separador-blanco.png" alt="Separador">
        <h2 class="pl-5">INSTALACIONES</h2>
        <p class="pl-5 text-justify">Airapí Memorial Park, es un modelo innovador en servicios funerarios, con altos estándares de calidad, instalaciones modernas diseñadas para dar un servicio de excelencia y cálido acompañamiento humano.</p>  
      </div>
      
    </div>
    <div class="carousel-item">
      <img class="d-block w-100" src="{{ asset('img/instalaciones/instalaciones_mainbanner_02.jpg') }}" alt="First slide">
      <div class="bannerText  col-md-5">
        <img class="pl-5" src="img/separador-blanco.png" alt="Separador">
        <h2 class="pl-5">INSTALACIONES</h2>
        <p class="pl-5 text-justify">En Airapí Memorial Park transformamos la atención, acompañamiento, y calidad de los servicios funerarios mediante el trabajo profesional  y dedicado de nuestros colaboradores, promoviendo la dignidad, el respeto y el tributo a la vida, ocupandonos de todo lo necesario para que estés tranquilo, confortado y rodeado por el afecto de quienes te acompañan.</p>  
      </div>
      
    </div>
    <div class="carousel-item">
      <img class="d-block w-100" src="{{ asset('img/instalaciones/instalaciones_mainbanner_03.jpg') }}" alt="Second slide">
      <div class="bannerText col-md-5">
      <img class="pl-5" src="img/separador-blanco.png" alt="Separador">
        <h2 class="pl-5">INSTALACIONES</h2>
        <p class="pl-5 text-justify">Contamos con las mejores instalaciones para ofrecerte un servicio único, innovador, humano y confortable. Te brindamos soluciones rápidas y eficaces en los momentos más difíciles.</p>  
      </div>
      
    </div>
    <div class="carousel-item">
      <img class="d-block w-100" src="{{ asset('img/instalaciones/instalaciones_mainbanner_04.jpg') }}" alt="Fourth slide">
      <div class="bannerText col-md-5">
      <img class="pl-5" src="img/separador-blanco.png" alt="Separador">
        <h2 class="pl-5">INSTALACIONES</h2>
        <p class="pl-5 text-justify">Quédate con la tranquilidad de contar con el mejor lugar, en los momentos más difíciles. Nuestras instalaciones son únicas e innovadoras para brindar un servicio en conceptos funerarios fuera de lo habitual.</p>  
      </div>
      
    </div>
  </div>
  <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>

<img class="pl-5 col-md-3 ml-3 w-100 col-8" src="img/separador-titulo.png" alt="Separador">
<h3 class="purple QuercusMedium ml-3 pl-5">MEMORIAL PARK</h3>



  <div class="card noBorder">
  
      <div id="carouselExampleIndicators2" class="carousel slide" data-ride="carousel">
      <ol class="carousel-indicators">
        <li data-target="#carouselExampleIndicators2" data-slide-to="0" class="active"></li>
        <li data-target="#carouselExampleIndicators2" data-slide-to="1"></li>
      </ol>
        <div class="carousel-item active p-4">
            <div class="row">
                <div class="col-md-4">
                  <button type="button" class="btn btn-primary pic" id="pic2" data-toggle="modal" data-target=".bd-example-modal-lg" style="background:transparent;border:0px;">  
                    <img class="w-100" src="{{ url ('img/instalaciones/instalaciones_galeria_01.jpg') }}" alt="">
                  </button>
                </div>
                <div class="col-md-4">
                    <button type="button" class="btn btn-primary pic" id="pic3" data-toggle="modal" data-target=".bd-example-modal-lg" style="background:transparent;border:0px;">  
                      <img class="w-100" src="{{ url ('img/instalaciones/instalaciones_galeria_02.jpg') }}" alt="">
                    </button>
                </div>
                <div class="col-md-4">
                    <button type="button" class="btn btn-primary pic" id="pic4" data-toggle="modal" data-target=".bd-example-modal-lg" style="background:transparent;border:0px;">  
                      <img class="w-100" src="{{ url ('img/instalaciones/instalaciones_galeria_03.jpg') }}" alt="">
                    </button>
                </div>
            </div>
        </div>
        <div class="carousel-item p-4">
            <div class="row">
              <div class="col-md-4">
                <button type="button" class="btn btn-primary pic" id="pic2" data-toggle="modal" data-target=".bd-example-modal-lg" style="background:transparent;border:0px;">  
                  <img class="w-100" src="{{ url ('img/instalaciones/instalaciones_galeria_04.jpg') }}" alt="">
                </button>
              </div>
              <div class="col-md-4">
                <button type="button" class="btn btn-primary pic" id="pic2" data-toggle="modal" data-target=".bd-example-modal-lg" style="background:transparent;border:0px;">  
                  <img class="w-100" src="{{ url ('img/instalaciones/instalaciones_galeria_05.jpg') }}" alt="">
                </button>
              </div>
              <div class="col-md-4">
               <button type="button" class="btn btn-primary pic" id="pic2" data-toggle="modal" data-target=".bd-example-modal-lg" style="background:transparent;border:0px;">  
                  <img class="w-100" src="{{ url ('img/instalaciones/instalaciones_galeria_06.jpg') }}" alt="">
                </button>
              </div>
            </div>
        </div>

      <a class="carousel-control-prev" href="#carouselExampleIndicators2" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="carousel-control-next" href="#carouselExampleIndicators2" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>
    </div>

    <div class="card-body bgYellowServicios">
  
        <div class="row p-4">
        <p class="hidden">{{ $i = 0 }}</p>
        @foreach($lista2->chunk(4) as $row)
            <div class="col-md-4  gpo-{{$i++}}">
              <ul class="list-group">              
              @foreach($row as $item)
                  <li class="list-group-item pb-1 pt-1"> <img src="img/purple_bullet.png" alt="AirapiBullet"> {{$item->texto}}</li>
                  <p class="m-0"><img class="w-100 col-md-8" src="img/separador-morado.png" alt="Separador"></p>
                @endforeach
              </ul>
            </div>
            @endforeach
        </div>
        
    </div>
  
  </div>

  <div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
    <div id="carouselExampleIndicators3" class="carousel slide" data-ride="carousel" id="carousel">
      <div class="carousel-inner">
          <div class="carousel-item cont" id="cont-pic1">
            <img class="w-100" src="{{ url ('img/instalaciones/instalaciones_galeria_01.jpg') }}" alt="">
          </div>
          <div class="carousel-item cont" id="cont-pic2">
            <img class="w-100" src="{{ url ('img/instalaciones/instalaciones_galeria_02.jpg') }}" alt="">
          </div>
          <div class="carousel-item cont" id="cont-pic3">
            <img class="w-100" src="{{ url ('img/instalaciones/instalaciones_galeria_03.jpg') }}" alt="">
          </div>
          <div class="carousel-item cont" id="cont-pic4">
            <img class="w-100" src="{{ url ('img/instalaciones/instalaciones_galeria_04.jpg') }}" alt="">
          </div>
          <div class="carousel-item cont" id="cont-pic5">
            <img class="w-100" src="{{ url ('img/instalaciones/instalaciones_galeria_05.jpg') }}" alt="">
          </div>
          <div class="carousel-item cont" id="cont-pic6">
            <img class="w-100" src="{{ url ('img/instalaciones/instalaciones_galeria_06.jpg') }}" alt="">
        </div> 
      </div>
      <a class="carousel-control-prev" href="#carouselExampleIndicators3" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="carousel-control-next" href="#carouselExampleIndicators3" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>
    </div>
    </div>
  </div>
</div>

<script>

$(document).ready(function(){

  $('#carousel-0').addClass('active');
  
  $('.pic').click(function(){
    $id = $(this).attr('id');
    $('.cont').removeClass('active');
    $('#cont-'+$id).addClass('active');
    
    console.log('#cont-'+$id);

  })
});

$('#carousel').carousel({
  interval: 5000
})
</script>


@endsection





