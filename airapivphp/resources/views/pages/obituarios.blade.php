@extends('..layouts.admin')


@section('content')

<div class="col-md-10 offset-md-1 pt-5"> 
    <div class="card mt-5 mb-5">

        <div class="card-header">
            <h2> Página: Obituario </h2>
        </div>

        <div class="card-body">
            <div class="row">
            @foreach ($obituarios as $obituario)
                <div class="col-md-4 pb-3">
                <form action="{{ url('obituario/'.$obituario->id) }}" method="post" id="form-{{ $obituario->id }}" enctype="multipart/form-data">
                {{ csrf_field() }}
                    <div class="card" id="card-form-{{ $obituario->id }}">
                        <div class="card-body"><div class="card">
                                <img class="w-100" id="img-{{$obituario->id}}" src="{{ url('obituario/'.$obituario->photoPersona ) }}" alt="">
                            </div>
                            <div class="form-group">
                            <label for=""></label>
                                <input type="hidden" class="form-control" value="{{$obituario->photoPersona}}" name="oldPhotoPersona">
                                <input type="file" class="form-control" id="file-{{$obituario->id}}" name="photoPersona">
                            </div>

                            <div class="form-group">
                                <label for="">Nombre</label>
                                <input type="text" class="form-control" value="{{$obituario->nombre}}" name="nombre">
                            </div>
                            <div class="form-group">
                                <label for="">Apellidos</label>
                                <input type="text" class="form-control" value="{{$obituario->apellidos}}" name="apellidos">
                            </div>
                            <div class="form-group">
                                <label for="">Sala</label>
                                <input type="text" class="form-control" value="{{$obituario->ubicacion}}" name="ubicacion">
                            </div>
                            <div class="form-group">
                                <label for="">Fecha Evento</label>
                                <input type="date" class="form-control" value="{{$obituario->fechaEvento}}" name="fechaEvento">
                            </div>
                            <div class="form-group">
                                <label for="">Descripción</label>
                                <textarea type="text" class="form-control" name="descripcionEvento" rows="5">{{$obituario->descripcionEvento}}</textarea>
                            
                            </div>
                            <div class="form-group">
                                <label for="">Horario Evento</label>
                                <input type="text" class="form-control" value="{{$obituario->horarioEvento}}" name="horarioEvento">
                            </div>

                            <div class="form-group action" id="{{$obituario->statusEvento}}">
                               @if($obituario->statusEvento)
                               <div class="div-{{$obituario->id}}">
                                    <span class="badge badge-success"> Activo</span>
                                    <button type="button"class="btn btn-outline-danger btn-status" data-status="0" id="{{$obituario->id}}"> Desactivar</button>
                               </div>
                               
                               @else
                                <div class="div-{{$obituario->id}}">
                                    <span class="badge badge-danger">Inactivo</span>
                                    <button type="button" class="btn btn-outline-success btn-status" data-status="1" id="{{$obituario->id}}"> Activar</button>
                                </div>
                               @endif
                            </div>
                        </div>
                        <div class="card-footer">
                        <button class="btn btn-primary" id="actualizar" data-update="form-{{$obituario->id}}">Guardar</button>
                        </div>
                    </div>
                </form>
                </div>
            @endforeach
            </div>
        </div>

        
<script>

$(document).on('click','.btn-status', function(e){
        e.preventDefault();
       id = $(this).attr('id');
       status = $(this).attr('data-status');
       $.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
       $.ajax({
        url: "{{ url('obituario/status') }}"+'/'+id,
        method: 'post',
        data: {
            statusEvento: status,
        },

        success: function(result){
           $('.div-'+ id).html('');
           console.log(status);
           if(status == 1){
            console.log('Activar'+status);
            $('.div-'+ id).append("<span class='badge badge-success'> Activo</span> <button type='button'class='btn btn-outline-danger btn-status' data-status='0' id='"+id+"'> Desactivar</button>");
           }
           if(status == 0){
            console.log('Desactivar'+status);;
            $('.div-'+ id).append("<span class='badge badge-danger'>Inactivo</span> <button type='button' class='btn btn-outline-success btn-status' data-status='1' id='"+id+"'> Activar</button>");
           }
        }});
    });


        $(document).on('click','#actualizar', function(e){
            e.preventDefault();
            form = $(this).attr('data-update');
            var formData = new FormData(document.getElementById(form));
            url = form.split('-');
            $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
            $.ajax({
                url: 'obituario/'+url[1],
                type: "post",
                dataType: "html",
                data: formData,
                cache: false,
                contentType: false,
	        processData: false
            })
                .done(function(res){
            //        
                    res = JSON.parse(res);
                    $('#file-'+res.id).val('');
                    $('#img-'+res.id).attr('src', 'obituario/' +res.photoPersona);
                });
        });



    // $(document).on('click','#actualizar', function(e){
    //     e.preventDefault();
    //     form = $(this).attr('data-update');
    //     var data = $('#'+form).serialize();
    //     console.log(data['photoPersona']);
      
    //     url = form.split('-');
    //     $.ajax({
    //         data: data,
    //         type: "post",
    //         url: 'obituario/'+url[1],
    //         cache: false,
    //         success: function(result){
    //         console.log(result);
    //         // $('#img-'+result['id']).attr('src', 'obituario/' +result['photoPersona']);
    //         // console.log($('#img-'+result['id']).attr('src'));

    //        // $('#card-'+form).empty();


    //            //$('#card-'+form).append("<div class='card-body'><div class='card'><img class='w-100' src='' alt='' ></div><div class='form-group'><label for=''></label>    <input type='file' class='form-control'  value="+data['photoPersona']+" name='photoPersona'></div> <div class='form-group'>    <label for=''>Nombre Completo</label>    <input type='text' class='form-control' value="+data['photoPersona']+" name='nombreCompleto'></div><div class='form-group'>    <label for=''>Fecha Evento</label>    <input type='date' class='form-control' value="+data['photoPersona']+" name='fechaEvento'></div><div class='form-group'>    <label for=''>Descripción</label>    <textarea type='text' class='form-control' name="+data['photoPersona']+" rows='5'>{{$obituario->descripcionEvento}}</textarea></div><div class='form-group'>    <label for=''>Horario Evento</label>    <input type='text' class='form-control' value="+data['photoPersona']+" name='horarioEvento'></div> <div class='form-group action' id="+data['statusEvento']+">  "if(data['statusEvento']){"   <div class='div-'" + data['id'] + ">        <span class='badge badge-success'> Activo</span>        <button type='button'class='btn btn-outline-danger btn-status' data-status='0' id="+data['id']+"> Desactivar</button>   </div>      "}else{"    <div class='div-'"+data['id']+">        <span class='badge badge-danger'>Inactivo</span>        <button type='button' class='btn btn-outline-success btn-status' data-status='1' id="data['id']"> Activar</button>    </div>  "}"</div></div><div class='card-footer'><input type='submit' value='Guardar' class='btn btn-primary' id='actualizar' data-update='form-'" + data['id']"></div>");

    //     }
    // });

    // });


</script>



@endsection