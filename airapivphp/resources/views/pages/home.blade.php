
@extends('..layouts.admin')


@section('content')

<div class="col-md-10 offset-md-1 pt-5"> 
    <div class="card mt-5 mb-5">

        <div class="card-header">
            <h2> Página: Home </h2>
        </div>

        <div class="card-body">
        <section class="card">
                    <div class="card-body">
                        <h2>SEO Home</h2>
                        <div class="form-group">
                        <label for="descripcion">Meta Título <small>(Máximo de 70 caracteres)</small></label>
                            <input maxlength="70" type="text" class="form-control" name="title" value="{{$home->title}}">
                        </div>

                        <div class="form-group">
                        <label for="descripcion">Descripción <small>(Máximo de 155 caracteres)</small></label>
                            <textarea maxlength="155" type="text" class="form-control" name="description">{{$home->description}}</textarea>
                        </div>

                        <div class="form-group">
                        <label for="descripcion">Keywords <small>(3 Keywords mínimo)</small></label>
                            <textarea type="text" class="form-control" name="keywords">{{$home->keywords}}</textarea>
                        </div>

                        <div class="form-group">
                            <input type="submit" value="Guardar" class="btn btn-primary" id="btnSeo">
                        </div>
                    </div>          
            </section>


            <section class="card">
                <div class="card-body">
                    <h3>Carrousel Principal</h3>
                    <hr> 
                    <div class="pt-5 pb-5">
                        <div class="row">
                        @foreach($carousel as $car1)
                        <div class="col-md-4" id="card-{{$car1->id}}">
                            <div class="card">
                                <div class="card-body">
                                    <img src="{{url('home/'.$car1->photoURL ) }}" name="{{$car1->photoURL}}" alt="" class="w-100">
                                </div>
                                <div class="card-footer text-center">
                                    <button class="btn btn-danger btnEliminarImg" id="{{$car1->id}}" name="{{$car1->photoURL}}">Eliminar</button>
                                </div>
                            </div>
                        </div>
                        @endforeach
                        </div>
                        <br>
                        <div class="form-group">
                            <form action="{{ url('/photo') }}" method="post" enctype="multipart/form-data">
                            {{ csrf_field() }}
                                <label for="carrousel">Agregar Fotos a Carrousel Home</label>
                                <input type="hidden" value="1" name="id">
                                <input type="file" class="form-control col-md-6" name="carrousel[]" multiple>
                                <input type="submit" class="btn btn-primary m-2" value="Subir Foto">
                            </form>
                        </div>      
                    </div>
                </div>
            </section>
            
            <section class="card">
                <div class="card-body">
                    <div class="pt-3 pb-3">
                        <h2>Textos de carrousel principal</h2>
                        <ul class="list-group"  id="lista">
                            @foreach($lista as $list)
                            <li class="list-group-item" id="txt-{{$list->id}}">{{ $list->texto }} <button class="btnEliminarText btn btn-danger text-right" id="{{ $list->id }}">Eliminar</button></li>                
                            @endforeach
                        </ul>
                    </div>

                    <div class="form-group">
                       
                        <input type="hidden" class="idlista" value="9">
                        <label for="car">Agregar Texto a Carrousel</label>
                        <input type="text" class="form-control" id="txtlista" required>
                        <button class="btn btn-primary m-2 btnLista" name="lista" value="Agregar">Agregar</button>
                    </div> 
                </div>
            </section>
            <hr>


            <section class="card">
                <div class="card-body">
                    <form action="{{url('/home')}}" method="post" enctype="multipart/form-data">
                    <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                    <div class="form-group">
                        <label for="nosotrosTxt">Texto Nosotros</label>
                        <textarea type="text" class="form-control" id="nosotrosTxt" rows="6" name="nosotrosTxt">{{$home->nosotrosTxt}}</textarea>
                    </div>
                    <div class="card">
                        <div class="card-body">
                        <img src="{{url('home/'.$home->nosotrosBG ) }}" name="{{$home->nosotrosBG}}" alt="" class="w-100">
                        </div>
                    </div>
                    <div class="form-group">                           
                        <label for="nosotrosBG">Fondo Nosotros</label>
                        <input type="file" class="form-control col-md-6" value="{{$home->nosotrosBG}}" name="nosotrosBG">
                    </div>     
                </div>
            </section>
            <hr>

            </form>  
        </div>
    </div>    
</div>


<script>
$(document).ready(function(){
});

$(document).on('click','.btnEliminarImg', function(e){
    e.preventDefault();
    var id = $(this).attr('id');
    var name = $(this).attr('name');

    $.ajax({
        url: "{{ url('/photo') }}"+'/'+id,
        method: 'get',
        data: {
            "_token":"{{ csrf_token() }}"
        },

        success: function(result){

            $('#card-'+result).remove();
        }});
    });

$(document).on('click','.btnLista', function(e){
    e.preventDefault();
    var name = $(this).attr('name');

    $.ajax({
        url: "{{ url('/texto') }}",
        method: 'post',

        data: {
            idInstalacion: $('.id'+name).val(),
            carrouselTxt:  $('#txt'+name).val(),
            "_token":"{{ csrf_token() }}"
        },
        success: function(result){ 
            var car = "<li class='list-group-item' id=txt-"+result+">" + $('#txt'+name).val() + " <button class='btnEliminarText btn btn-danger' id='"+result+"'>Eliminar</button></li>  </li>";
            $('#'+name).append(car);
            $('#txt'+name).val('');
        }});
    });


    $(document).on('click','.btnEliminarText', function(e){
    e.preventDefault();
    var id = $(this).attr('id');
    $.ajax({   
        url: "{{ url('/texto') }}"+'/'+id,
        method: 'get',
        

        success: function(result){
            $('#txt-'+id).remove();
        }});
    });
    
    CKEDITOR.replace( 'nosotrosTxt' );

</script>



@endsection