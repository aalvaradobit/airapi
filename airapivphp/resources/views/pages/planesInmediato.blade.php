@extends('..layouts.admin')


@section('content')

<div class="col-md-10 offset-md-1 pt-5"> 
    <div class="card mt-5 mb-5">
        <div class="card-header">
            <h2> Página: Planes Uso Inmediato </h2>
        </div>
        <div class="card-body">
            <section class="card">
                <div class="card-body">
                    <h2>SEO Instalaciones</h2>
                  
                    {{ csrf_field() }}
                    <div class="form-group">
                    <label for="descripcion">Meta Título <small>(Máximo de 70 caracteres)</small></label>
                        <input maxlength="70" type="text" class="form-control" name="title" id="title" value="{{$planesI->title}}">
                    </div>

                    <div class="form-group">
                    <label for="descripcion">Descripción <small>(Máximo de 155 caracteres)</small></label>
                        <textarea maxlength="155" type="text" class="form-control" name="description" id="description">{{$planesI->description}}</textarea>
                    </div>

                    <div class="form-group">
                    <label for="descripcion">Keywords <small>(3 Keywords mínimo)</small></label>
                        <textarea type="text" class="form-control" name="keywords" id="keywords">{{$planesI->keywords}}</textarea>
                    </div>

                    <div class="form-group">
                        <button type="submit" id="guardarSeo" class="btn btn-primary">Guardar</button>
                    </div>
                    </form>
                </div>          
            </section>

            <section class="card">
                    <div class="card-body">
                            <h2>Servicios a seleccionar</h2>
                            <div class="row">
                                <div class="col-md-10 offset-md-1">
                                    <ul class="list-group" id="lista">
                                        @foreach($lista as $list)
                                        <li class="list-group-item" id="txt-{{$list->id}}">{{ $list->texto }} <button class="btnEliminarText btn btn-danger" id="{{ $list->id }}">Eliminar</button></li>                
                                        @endforeach
                                    </ul>
                                </div>    
                            </div>
                            

                            <div class="form-group">
                            
                                <input type="hidden" class="idlista" value="1">
                                <label for="">Agregar Elementos a la lista</label>
                                <input type="text" class="form-control" id="txtlista" required>
                                <button class="btn btn-primary m-2 btnLista" name="lista" value="Agregar">Agregar</button>
                            </div> 
                        </div>
                </section>

                <section class="card">
                <div class="card-body">
                        <h2>Todos nuestros planes incluyen:</h2>
                        <div class="row">
                            <div class="col-md-10 offset-md-1">
                                <ul class="list-group" id="lista2">
                                    @foreach($lista2 as $list2)
                                    <li class="list-group-item" id="txt-{{$list2->id}}">{{ $list2->texto }} <button class="btnEliminarText btn btn-danger" id="{{ $list2->id }}">Eliminar</button></li>                
                                    @endforeach
                                </ul>
                            </div>    
                        </div>
                        

                        <div class="form-group">
                          
                            <input type="hidden" class="idlista2" value="2">
                            <label for="">Agregar Elementos a la lista</label>
                            <input type="text" class="form-control" id="txtlista2" required>
                            <button class="btn btn-primary m-2 btnLista" name="lista2" value="Agregar">Agregar</button>
                        </div> 
                    </div>
            </section>
            </div>
 

    </div>
</div>
        
<script>
    $(document).on('click','#guardarSeo', function(e){
    e.preventDefault();
    var title = $('#title').val();
    var description = $('#description').val();
    var keywords = $('#keywords').val();

    $.ajax({
        url: "{{ url('/planesInmediato') }}",
        method: 'post',

        data: {
            title : title,
            description : description,
            keywords : keywords,
            "_token":"{{ csrf_token() }}"
        },
        success: function(result){ 

          $('#Actualizado').first().fadeIn('slow').delay(2000).fadeOut('slow');
          
        }});
    });



    $(document).on('click','.btnLista', function(e){
    e.preventDefault();
    var name = $(this).attr('name');

    $.ajax({
        url: "{{ url('/texto') }}",
        method: 'post',

        data: {
            idInstalacion: $('.id'+name).val(),
            carrouselTxt:  $('#txt'+name).val(),
            "_token":"{{ csrf_token() }}"
        },
        success: function(result){ 
            $('#Agregado').first().fadeIn('slow').delay(500).fadeOut('slow');

            var car = "<li class='list-group-item' id=txt-"+result+">" + $('#txt'+name).val() + " <button class='btnEliminarText btn btn-danger' id='"+result+"'>Eliminar</button></li>  </li>";
            $('#'+name).append(car);
            $('#txt'+name).val('');
        }});
    });


    $(document).on('click','.btnEliminarText', function(e){
    e.preventDefault();
    var id = $(this).attr('id');
    $.ajax({   
        url: "{{ url('/texto') }}"+'/'+id,
        method: 'get',
        

        success: function(result){

            $('#Eliminado').first().fadeIn('slow').delay(500).fadeOut('slow');
            $('#txt-'+id).remove();
        }});
    });


</script>



@endsection