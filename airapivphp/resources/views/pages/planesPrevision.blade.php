@extends('..layouts.admin')


@section('content')

<div class="col-md-10 offset-md-1 pt-5"> 
    <div class="card mt-5 mb-5">
        <div class="card-header">
            <h2> Página: Planes Uso Inmediato </h2>
        </div>
        <div class="card-body">
            <section class="card">
                <div class="card-body">
                    <h2>SEO Planes Previsión</h2>
                  
                    {{ csrf_field() }}
                    <div class="form-group">
                    <label for="descripcion">Meta Título <small>(Máximo de 70 caracteres)</small></label>
                        <input maxlength="70" type="text" class="form-control" name="title" id="title" value="{{$prevision->title}}">
                    </div>

                    <div class="form-group">
                    <label for="descripcion">Descripción <small>(Máximo de 155 caracteres)</small></label>
                        <textarea maxlength="155" type="text" class="form-control" name="description" id="description">{{$prevision->description}}</textarea>
                    </div>

                    <div class="form-group">
                    <label for="descripcion">Keywords <small>(3 Keywords mínimo)</small></label>
                        <textarea type="text" class="form-control" name="keywords" id="keywords">{{$prevision->keywords}}</textarea>
                    </div>

                    <div class="form-group">
                        <button type="submit" id="guardarSeo" class="btn btn-primary">Actualizar Seo</button>
                    </div>
                    </form>
                </div>          
            </section>

            <section class="card">
                    <div class="card-body">
                        <div class="form-group">
                        <form method="post" action="{{ url ('planesPrevision/Portada') }}" enctype="multipart/form-data" id="formPhoto">
                            <label for="">Portada</label>
                            <div class="card col-md-4 mb-3">
                                <img class="w-100" id="img" src="{{url('prevision/'.$prevision->imgPortada)}}" alt="">
                            </div>
                            <input type="file" id="file" class="form-control" name="imgPortada" value="{{$prevision->imgPortada}}">
                        </div>
                            <input type="submit" value="Subir Foto"  class="btn btn-primary" id="actualizarPhoto">
                        </form>
                    </div>
                </section>

            <section class="card">
                    <div class="card-body">
                    <img src="" alt="">
                            <h2>Beneficios</h2>
                            <div class="row">
                                <div class="col-md-10 offset-md-1">
                                    <ul class="list-group" id="lista">
                                        @foreach($lista as $list)
                                        <li class="list-group-item" id="txt-{{$list->id}}">{{ $list->texto }} <button class="btnEliminarText btn btn-danger" id="{{ $list->id }}">Eliminar</button></li>                
                                        @endforeach
                                    </ul>
                                </div>    
                            </div>
                            

                            <div class="form-group">
                            
                                <input type="hidden" class="idlista" value="7">
                                <label for="">Agregar Elementos a la lista</label>
                                <input type="text" class="form-control" id="txtlista" required>
                                <button class="btn btn-primary m-2 btnLista" name="lista" value="Agregar">Agregar</button>
                            </div> 
                        </div>
                </section>

                <section class="card">
                <div class="card-body">
                        <h2>Todos nuestros planes incluyen:</h2>
                        <div class="row">
                            <div class="col-md-10 offset-md-1">
                                <ul class="list-group" id="lista2">
                                    @foreach($lista2 as $list2)
                                    <li class="list-group-item" id="txt-{{$list2->id}}">{{ $list2->texto }} <button class="btnEliminarText btn btn-danger" id="{{ $list2->id }}">Eliminar</button></li>                
                                    @endforeach
                                </ul>
                            </div>    
                        </div>
                        

                        <div class="form-group">
                          
                            <input type="hidden" class="idlista2" value="8">
                            <label for="">Agregar Elementos a la lista</label>
                            <input type="text" class="form-control" id="txtlista2" required>
                            <button class="btn btn-primary m-2 btnLista" name="lista2" value="Agregar">Agregar</button>
                        </div> 
                    </div>
            </section>

            <div class="planesCard pt-5 pb-5">
                <div class="card noBorder">
                    <div class="card-body">
                    
                            <div class="row">
                            @foreach($planes as $plan)
                                <div class="col-md-4 pb-5">
                                    <div class="card">
                                        <div class="card-body">
                                            <form action="" method="POST" id="form-{{$plan->id}}">
                                                <label for="">Nombre del Plan:</label>
                                                <input class="form-control" name="descripcionPlan" value="{{$plan->descripcionPlan}}" name="descriptionPlan">
                                                <label for="">Pago de contado descuento del: </label>
                                                <input class="form-control" name="descuento" value="{{$plan->descuento}}"> 
                                                <label for="">Paga a 12 Meses descuento del: </label>
                                                <input class="form-control" name="descuento12" value="{{$plan->descuento12}}">
                                                <label for="">Paga a 24 Meses descuento del: </label>                                            
                                                <input class="form-control" name="descuento24" value="{{$plan->descuento24}}">
                                                <label for="">Costo</label>
                                                <input class="form-control" name="costo" value="{{$plan->costo}}">
                                                <div class="form-group">
                                                    <input type="submit" id="updatePlan" class="form-control btn btn-primary" value="Actualizar Plan" data-id="form-{{$plan->id}}">
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                            </div>
                       
                    </div>
                </div>
            </div>

        </div>
 

    </div>
</div>



        
<script>



    $(document).on('click','#guardarSeo', function(e){
    e.preventDefault();
    var title = $('#title').val();
    var description = $('#description').val();
    var keywords = $('#keywords').val();

    $.ajax({
        url: "{{ url('/planesPrevision/Seo') }}",
        method: 'post',

        data: {
            title : title,
            description : description,
            keywords : keywords,
            "_token":"{{ csrf_token() }}"
        },
        success: function(result){ 
          console.log(result);
          $('#Actualizado').first().fadeIn('slow').delay(2000).fadeOut('slow');
        }});
    });



    $(document).on('click','.btnLista', function(e){
    e.preventDefault();
    var name = $(this).attr('name');

    $.ajax({
        url: "{{ url('/texto') }}",
        method: 'post',

        data: {
            idInstalacion: $('.id'+name).val(),
            carrouselTxt:  $('#txt'+name).val(),
            "_token":"{{ csrf_token() }}"
        },
        success: function(result){ 
            $('#Agregado').first().fadeIn('slow').delay(500).fadeOut('slow');

            var car = "<li class='list-group-item' id=txt-"+result+">" + $('#txt'+name).val() + " <button class='btnEliminarText btn btn-danger' id='"+result+"'>Eliminar</button></li>  </li>";
            $('#'+name).append(car);
            $('#txt'+name).val('');
         
            
        }});
    });


    $(document).on('click','.btnEliminarText', function(e){
    e.preventDefault();
    var id = $(this).attr('id');
    $.ajax({   
        url: "{{ url('/texto') }}"+'/'+id,
        method: 'get',
        

        success: function(result){
            $('#Eliminado').first().fadeIn('slow').delay(500).fadeOut('slow');
            $('#txt-'+id).remove();
        }});
    });


    $(document).on('click','#actualizarPhoto', function(e){
            e.preventDefault();
            var formData = new FormData(document.getElementById('formPhoto'));
           
            $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
            $.ajax({
                url: 'planesPrevision/Portada',
                type: "post",
                dataType: "html",
                data: formData,
                cache: false,
                contentType: false,
	            processData: false
            })
                .done(function(res){
                    $('#Actualizado').first().fadeIn('slow').delay(2000).fadeOut('slow');
                    console.log(res); 
                    res = JSON.parse(res);
                    $('#file').val('');
                    $('#img').attr('src', 'prevision/' +res.imgPortada);
                    
                });
        });


        $(document).on('click','#updatePlan', function(e){
            e.preventDefault();

            form = $(this).attr('data-id');

            var formData = new FormData(document.getElementById(form));
            var url = form.split('-');
            $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
            $.ajax({
                url: 'planesPrevision/'+ url[1],
                type: "post",
                dataType: "html",
                data: formData,
                cache: false,
                contentType: false,
	            processData: false
            })
                .done(function(res){
                    $('#Actualizado').first().fadeIn('slow').delay(2000).fadeOut('slow');
                });
        });


</script>



@endsection