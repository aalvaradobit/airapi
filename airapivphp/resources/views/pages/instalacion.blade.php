
@extends('..layouts.admin')


@section('content')

<div class="col-md-10 offset-md-1 pt-5"> 
    <div class="card mt-5 mb-5">
        <div class="card-header">
            <h2> Página: {{ $instalaciones->descripcion }} </h2>
        </div>

        <div class="card-body">

            <section class="card">
                <div class="card-body">
                    <h2>SEO Instalaciones</h2>
                    <form action="{{url('/instalacion')}}" method="post" >
                    {{ csrf_field() }}
                    <div class="form-group">
                    <label for="descripcion">Meta Título <small>(Máximo de 70 caracteres)</small></label>
                        <input maxlength="70" type="text" class="form-control" name="title" value="{{$instalaciones->title}}">
                    </div>

                    <div class="form-group">
                    <label for="descripcion">Descripción <small>(Máximo de 155 caracteres)</small></label>
                        <textarea maxlength="155" type="text" class="form-control" name="description">{{$instalaciones->description}}</textarea>
                    </div>

                    <div class="form-group">
                    <label for="descripcion">Keywords <small>(3 Keywords mínimo)</small></label>
                        <textarea type="text" class="form-control" name="keywords">{{$instalaciones->keywords}}</textarea>
                    </div>

                    <div class="form-group">
                        <input type="submit" value="Guardar" class="btn btn-primary">
                    </div>
                    </form>
                </div>          
            </section>
            <hr>
           <section class="card">
                <div class="card-body">
                    <h2>Carrousel Principal</h2>
                    <hr>      
                    <div class="pt-5 pb-5">
                        <div class="row">
                        @foreach($carousel1 as $car1)
                        <div class="col-md-4" id="card-{{$car1->id}}">
                            <div class="card">
                                <div class="card-body">
                                    <img src="{{url('instalacion/'.$car1->photoURL ) }}" name="{{$car1->photoURL}}" alt="" class="w-100">
                                </div>
                                <div class="card-footer text-center">
                                    <button class="btn btn-danger btnEliminarImg" id="{{$car1->id}}" name="{{$car1->photoURL}}">Eliminar</button>
                                </div>
                            </div>
                        </div>
                        @endforeach
                        </div>
                        <br>
                        <div class="form-group">
                            <form action="{{ url('/photo') }}" method="post" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                    <label for="carrousel">Agregar Fotos a Carrousel Instalaciones</label>
                                    <input type="hidden" value="2" name="id">
                                    <input type="file" class="form-control" name="carrousel[]" multiple>
                                    <input type="submit" class="btn btn-primary m-2" value="Agregar">
                            </form>
                        </div>      
                    </div>
                </div>
           </section>
            <hr>
            <section class="card">
                <div class="card-body">
                    <div class="pt-3 pb-3">
                        <h2>Textos de carrousel principal</h2>
                        <ul class="list-group"  id="lista1">
                            @foreach($lista as $list1)
                            <li class="list-group-item" id="txt-{{$list1->id}}">{{ $list1->texto }} <button class="btnEliminarText btn btn-danger" id="{{ $list1->id }}">Eliminar</button></li>                
                            @endforeach
                        </ul>
                    </div>

                    <div class="form-group">
                       
                        <input type="hidden" class="idlista1" value="3">
                        <label for="car1">Agregar Texto a Carrousel</label>
                        <input type="text" class="form-control" id="txtlista1" required>
                        <button class="btn btn-primary m-2 btnLista" name="lista1" value="Agregar">Agregar</button>
                    </div> 
                </div>
            </section>
            <hr>
            <section class="card">
                <div class="card-body">
                    <h2>Carousel Galeria</h2>
                    <hr>
                    <div class="row">
                    @foreach($carousel2 as $car2)
                    <div class="col-md-4" id="card-{{$car2->id}}">
                        <div class="card">
                            <div class="card-body">
                                <img src="{{url('instalacion/'.$car2->photoURL ) }}" name="{{$car2->photoURL}}" alt="" class="w-100">
                            </div>
                            <div class="card-footer text-center">
                                <button class="btn btn-danger btnEliminarImg" id="{{$car2->id}}" name="{{$car2->photoURL}}">Eliminar</button>
                            </div>
                        </div>
                    </div>
                    @endforeach
                    </div>
                    <div class="form-group">
                        <form action="{{ url('/photo') }}" method="post" enctype="multipart/form-data">
                        {{ csrf_field() }}
                            <label for="carrousel">Agregar Fotos a Carrousel Galeria</label>
                            <input type="hidden" value="3" name="id">
                            <input type="file" class="form-control" name="carrousel[]" multiple>
                            <input type="submit" class="btn btn-primary m-2" value="Agregar">
                        </form>
                    </div>
                </div>
            </section>
            <hr>
            <section class="card">
                <div class="card-body">
                        <h2>Lista Inferior</h2>
                        <div class="row">
                            <div class="col-md-10 offset-md-1">
                                <ul class="list-group" id="lista2">
                                    @foreach($lista2 as $list2)
                                    <li class="list-group-item" id="txt-{{$list2->id}}">{{ $list2->texto }} <button class="btnEliminarText btn btn-danger" id="{{ $list2->id }}">Eliminar</button></li>                
                                    @endforeach
                                </ul>
                            </div>    
                        </div>
                        

                        <div class="form-group">
                          
                            <input type="hidden" class="idlista2" value="4">
                            <label for="">Agregar Elementos a la lista</label>
                            <input type="text" class="form-control" id="txtlista2" required>
                            <button class="btn btn-primary m-2 btnLista" name="lista2" value="Agregar">Agregar</button>
                        </div> 
                    </div>
            </section>
        </div>
    </div>    
</div>



<script>

$(document).ready(function(){

    $(document).on('click','.btnLista', function(e){
    e.preventDefault();
    var name = $(this).attr('name');

    $.ajax({
        url: "{{ url('/texto') }}",
        method: 'post',

        data: {
            idInstalacion: $('.id'+name).val(),
            carrouselTxt:  $('#txt'+name).val(),
            "_token":"{{ csrf_token() }}"
        },
        success: function(result){ 
            var car = "<li class='list-group-item' id=txt-"+result+">" + $('#txt'+name).val() + "<button class='btnEliminarText btn btn-danger' id='"+result+"'>Eliminar</button></li>  </li>";
            $('#'+name).append(car);
            $('#txt'+name).val('');
        }});
    });


    $(document).on('click','.btnEliminarText', function(e){
    e.preventDefault();
    var id = $(this).attr('id');
    $.ajax({   
        url: "{{ url('/texto') }}"+'/'+id,
        method: 'get',
        

        success: function(result){
            $('#txt-'+id).remove();
        }});
    });



    

    $(document).on('click','.btnEliminarImg', function(e){
    e.preventDefault();
    var id = $(this).attr('id');
    var name = $(this).attr('name');
    $.ajax({
        url: "{{ url('/photo') }}"+'/'+id,
        method: 'get',

        success: function(result){

            $('#card-'+result).remove();
        }});
    });

});
</script>


@endsection