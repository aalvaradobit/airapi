
@extends('..layouts.admin')


@section('content')

<div class="col-md-10 offset-md-1 pt-5"> 
    <div class="card mt-5 mb-5">
        <div class="card-header">
            <h2> Página: Servicios </h2>
        </div>

        <div class="card-body">

        <section class="card">
            <div class="card-body">
                <form action="{{url('/servicio') }}" method="post" enctype="multipart/form-data">
                    {{ csrf_field() }}
                        <div class="form-group">
                        <label for="">Portada</label>
                            <div class="card col-md-4 mb-3">
                                <img class="w-100" src="{{url('servicio/'.$servicios->portadaImg)}}" alt="">
                            </div>
                        
                            <input type="file" class="form-control" name="portadaImg" value="{{$servicios->portadaImg}}">
                        </div>
                        <div class="form-group">
                            <label for="">Texto Portada</label>
                            <textarea type="text" class="form-control" name="portadaTxt">{{$servicios->portadaTxt}}</textarea>
                        </div>
                        <div class="form-group">
                            <label for="">Texto Promoción</label>
                            <textarea type="text" class="form-control" name="promoTxt">{{$servicios->promoTxt}}</textarea>
                        </div>
                        <section class="card">
                            <div class="card-body">
                                <h2>SEO Servicios</h2>
                                
                                    <div class="form-group">
                                    <label for="descripcion">Meta Título <small>(Máximo de 70 caracteres)</small></label>
                                        <input maxlength="70" type="text" class="form-control" name="title" value="{{$servicios->title}}">
                                    </div>

                                    <div class="form-group">
                                    <label for="descripcion">Descripción <small>(Máximo de 155 caracteres)</small></label>
                                        <textarea maxlength="155" type="text" class="form-control" name="description">{{$servicios->description}}</textarea>
                                    </div>

                                    <div class="form-group">
                                    <label for="descripcion">Keywords <small>(3 Keywords mínimo)</small></label>
                                        <textarea type="text" class="form-control" name="keywords">{{$servicios->keywords}}</textarea>
                                    </div>                        
                            </div>          
                        </section>
                    <div class="form-group pt-2">
                        <input type="submit" value="Guardar" class="btn btn-primary">
                    </div>
                </form>
            </div>
        </section>


            <hr>

                <form action="{{url('/servicio2') }}" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}
                    <div class="form-group">
                        
                        <label for="">Imagen Servicio Adicional</label>
                        <div class="card col-md-4 mb-3">
                            <img class="w-100"  src="{{url('servicio/'.$servicios->imgAdicional)}}" alt="">
                        </div>
                        <input type="file" class="form-control" name="imgAdicional" value="{{$servicios->imgAdicional}}">
                    </div>
                    <input type="submit" value="Guardar" class="btn btn-primary">
                </form>

                <hr>

            <div class="pt-3 pb-3">
                <h2>Servicios</h2>
                <div class="row">
                    <div class="col-md-10 offset-md-1">
                        <ul class="list-group" id="lista3">
                            @foreach($lista as $list3)
                            <li class="list-group-item" id="txt-{{$list3->id}}">{{ $list3->texto }} <button class="btnEliminarText btn btn-danger" id="{{ $list3->id }}">Eliminar</button></li>                
                            @endforeach

                        </ul>
                    </div>    
                </div>
                <hr>

                <div class="form-group">
                        <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                        <input type="hidden" class="idlista3" value="5">
                        <label for="">Agregar Servicio</label>
                        <input type="text" class="form-control" id="txtlista3" required>
                        <button class="btn btn-primary m-2 btnLista" name="lista3" value="Agregar">Agregar</button>

                </div> 
            </div>
        
            <div class="pt-3 pb-3">
                <h2>Servicios Adicionales</h2>
                <div class="row">
                    <div class="col-md-10 offset-md-1">
                        <ul class="list-group" id="lista6">
                            @foreach($lista4 as $list6)
                            <li class="list-group-item" id="txt-{{$list6->id}}">{{ $list6->texto }} <button class="btnEliminarText btn btn-danger" id="{{ $list6->id }}">Eliminar</button></li>                
                            @endforeach
                        </ul>
                    </div>    
                </div>
                <hr>

                <div class="form-group">
                        <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                        <input type="hidden" class="idlista6" value="6">
                        <label for="">Agregar Servicio Adicional</label>
                        <input type="text" class="form-control" id="txtlista6" required>
                        <button class="btn btn-primary m-2 btnLista" name="lista6" value="Agregar">Agregar</button>

                </div> 
            </div>
           
        </div>
    </div>    
</div>

<script>

$(document).ready(function(){


});


    $(document).on('click','.btnLista', function(e){
    e.preventDefault();
    var name = $(this).attr('name');
    console.log(name);
    $.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
    $.ajax({
        "_token": $('#token').val(),
        url: "{{ url('/texto') }}",
        method: 'post',
        data: {
            idInstalacion: $('.id'+name).val(),
            carrouselTxt:  $('#txt'+name).val()
        },
        success: function(result){ 
            var car = "<li class='list-group-item' id=txt-"+result+">" + $('#txt'+name).val() + " <button class='btnEliminarText btn btn-danger' id='"+result+"'>Eliminar</button></li>  </li>";
            $('#'+name).append(car);
            $('#txt'+name).val('');
        }});
    });




    $(document).on('click','.btnEliminarText', function(e){
    e.preventDefault();
    var id = $(this).attr('id');
    $.ajax({   
        url: "{{ url('/texto') }}"+'/'+id,
        method: 'get',

        success: function(result){
            $('#txt-'+id).remove();
        }});
    });



CKEDITOR.replace( 'portadaTxt' );
</script>

@endsection