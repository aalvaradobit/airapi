@extends('..layouts.app')


@section('title', 'Funeraria con velatorio y crematorio en Querétaro, Airapi Memorial Park')
@section('keywords', 'funeraria en querétaro, servicios funerarios en querétaro, velatorios en querétaro, crematorios en querétaro, airapi')
@section('description', 'Servicios funenarrios con los mejores complementos y planes , velatorios, crematorios, recolección y traslado de cuerpo en Querétaro.')


@section('content')

<div class="container1" >
    <img src="{{asset('img/actividades/img_destacada_actividades.jpg')}}" alt="Servicios" class="w-100">
</div>


<img class="pl-4 pt-5" src="{{ asset('img/separador-titulo.png')}}" alt="Separador">
    <h2 class="pl-4 pb-5 title yellow QuercusBold">{{$servicio->titulo }}</h2>

<div class="col-md-10 offset-md-1 pt-5 pb-5 postText ">
    <div class="row">
        <div class="col-md-6">
            <p class="purple QuercusLight">Fecha: <font class="yellow QuercusMedium">  {{ $servicio->fecha}} </font> </p>
            <p class="purple QuercusLight"> Hora: <font class="yellow QuercusMedium">  {{ $servicio->horario}} </font> </p>
        </div>

            
        <div class="col-md-2 QuercusLight yellow text-right"><p>Informes:  </p></div>
        <div class="col-md-4">
            <p class="purple QuercusLight">Tel: <a class="yellow QuercusMedium" href="tel:4425063445">(442) 506 · 3445</a>  </p>
            <p class="purple QuercusLight">Correo: <a class="yellow QuercusMedium" href="mailto:info@airapi.com.mx">info@airapi.com.mx</a> </p>
        </div>
    </div>
    <div class="purple Quercus pt-5">{!!$servicio->descripcion!!}</div>
</div>
@endsection