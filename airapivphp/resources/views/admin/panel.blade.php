@extends('..layouts.admin')


@section('content')

<div class="col-md-10 offset-md-1"> 
    <div class="card mt-5">
        <div class="card-body">
            <div class="row">
                <div class="col-md-6">
                    <div class="card">
                        <div class="card-body">
                            <div class="card-header">
                                Actividades
                            </div>
                            <div class="card-body text-center">
                                <a href="{{ url('/addS') }}"class="btn btn-success">Agregar</a>
                                <a href="{{ url('/detalleS') }}" class="btn btn-primary">Ver Lista</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="card">
                        <div class="card-body">
                            <div class="card-header">
                                Tributos
                            </div>
                            <div class="card-body text-center">
                                <a href="{{ url('/addT') }}"class="btn btn-success">Agregar</a>
                                <a href="{{ url('/detallesT') }}" class="btn btn-primary">Ver Lista</a>
                            </div>
                        </div>
                    </div>
                </div>

                <div style="display:none" class="col-md-6">
                    <div class="card">
                        <div class="card-body">
                            <div class="card-header">
                                SEO
                            </div>
                            <div class="card-body text-center">
                                <!--a href="{{ url('/addSeo') }}"class="btn btn-success">Agregar</a-->
                                <a href="{{ url('/detalleSeo') }}" class="btn btn-primary">Ver Lista</a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="card">
                        <div class="card-body">
                            <div class="card-header">
                                Home
                            </div>
                            <div class="card-body text-center">
                                <a href="{{ url('/home') }}"class="btn btn-success">Modificar</a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="card">
                        <div class="card-body">
                            <div class="card-header">
                                Instalaciones
                            </div>
                            <div class="card-body text-center">
                                <a href="{{ url('/instalacion') }}"class="btn btn-success">Modificar</a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="card">
                        <div class="card-body">
                            <div class="card-header">
                                Servicios
                            </div>
                            <div class="card-body text-center">
                                <a href="{{ url('/servicio') }}"class="btn btn-success">Modificar</a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="card">
                        <div class="card-body">
                            <div class="card-header">
                                Obituarios
                            </div>
                            <div class="card-body text-center">
                                <a href="{{ url('/obituario') }}"class="btn btn-success">Modificar</a>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="col-md-6">
                    <div class="card">
                        <div class="card-body">
                            <div class="card-header">
                                Planes Inmediato
                            </div>
                            <div class="card-body text-center">
                                <a href="{{ url('/planesInmediato') }}"class="btn btn-success">Modificar</a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="card">
                        <div class="card-body">
                            <div class="card-header">
                                Planes Previsión
                            </div>
                            <div class="card-body text-center">
                                <a href="{{ url('/planesPrevision') }}"class="btn btn-success">Modificar</a>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
@endsection