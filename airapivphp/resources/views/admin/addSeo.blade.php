@extends('..layouts.admin')


@section('content')

<div class="col-md-10 offset-md-1"> 
    <div class="card mt-5 p-5">
        
            <form action="{{ url('addSeo') }}" method="post">
            {{ csrf_field() }}
                <div class="form-group">
                    <label for="page">Sección</label>
                        <select name="page" class="form-control">
                            <option value="home">Home</option>
                            <option value="tributos">Tributos</option>
                            <option value="instalaciones">Instalaciones</option>
                            <option value="servicios">Servicios</option>
                            <option value="contacto">Contacto</option>
                        </select>
                    </div>

                    <div class="form-group">
                    <label for="descripcion">Meta Título <small>(Máximo de 70 caracteres)</small></label>
                        <input maxlength="70" type="text" class="form-control" name="titulo">
                    </div>

                    <div class="form-group">
                    <label for="descripcion">Descripción <small>(Máximo de 155 caracteres)</small></label>
                        <textarea maxlength="155" type="text" class="form-control" name="description"></textarea>
                    </div>

                    <div class="form-group">
                    <label for="descripcion">Keywords <small>(3 Keywords mínimo)</small></label>
                        <textarea type="text" class="form-control" name="keywords"></textarea>
                    </div>

                    <div class="form-group">
                        <input type="submit" value="Enviar" class="btn btn-primary">
                    </div>
            </form>
        
    </div>
</div>


@endsection