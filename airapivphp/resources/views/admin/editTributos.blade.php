@extends('..layouts.admin')


@section('content')

<div class="col-md-10 offset-md-1"> 
    <div class="card mt-5 p-5">
        
            <form action="{{ url('/tributos/edit/'.$tributos->id) }}" method="post"  enctype="multipart/form-data">
            {{ csrf_field() }}
                <div class="form-group">
                    <label for="titulo">Titulo</label>
                        <input type="text" class="form-control" name="titulo" value="{{$tributos->titulo}}" required>
                    </div>
                    <div class="form-group">
                    <label for="descripcion">Descripcion</label>
                    <textarea name="editor1" id="editor1" rows="10" cols="80">
                            {{$tributos->descripcion}}
                    </textarea>
                    </div>
                    
                    <div class="form-group">
                        <img style="width:40px;" src="{{ url('image/'.$tributos->caratula) }}" alt="{{$tributos->caratula}}">
                    <label for="caratula">Caratula</label>

                    <input type="file" name="caratula" class="form-control" value="{{$tributos->caratula}}">
                    </div>

                    <div class="form-group">
                        <img style="width:40px;" src="{{ url('image/'.$tributos->portada) }}" alt="{{$tributos->titulo}}">
                    <label for="portada">Portada</label>

                    <input type="file" name="portada" class="form-control" value="{{$tributos->portada}}">
                    </div>

                    <div class="form-group">
                        <input type="submit" value="Enviar" class="btn btn-primary">
                    </div>
                    
            </form>
        
    </div>
</div>

<script>
                // Replace the <textarea id="editor1"> with a CKEditor
                // instance, using default configuration.
                CKEDITOR.replace( 'editor1' );
            </script>
@endsection