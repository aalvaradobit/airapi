@extends('..layouts.admin')


@section('content')

<div class="col-md-10 offset-md-1"> 
    <div class="card mt-5">
        <div class="card-body">
            <table class="table">
            <thead>
                <tr>
                <th scope="col">Página</th>
                <th scope="col">Meta Título</th>
                <th scope="col">Meta Descripción</th>
                <th scope="col">Meta Keywords</th>
                <th scope="col">Opciones</th>
                </tr>
            </thead>
            <tbody>
                @foreach($seos as $seo)
                <tr>
                <td>{{$seo->page}}</td>
                <td>{{$seo->title}}</td>
                <td>{{$seo->description}}</td>
                <td>{{$seo->keywords}}</td>
                <td><a class="btn btn-warning white" href="{{ url('seo/edit/'.$seo->id) }}"> Modificar </a></td>
                </tr>
                @endforeach
            </tbody>
            </table>
        </div>
    </div>
</div>
@endsection