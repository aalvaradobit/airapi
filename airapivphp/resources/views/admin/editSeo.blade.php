@extends('..layouts.admin')


@section('content')

<div class="col-md-10 offset-md-1"> 
    <div class="card mt-5 p-5">
        
    <form action="{{ url('editSeo/'.$seo->id) }}" method="post">
            {{ csrf_field() }}
                <div class="form-group">
                    <label for="page">Página: {{ $seo->page }}</label>
                        <input type="hidden" name="page" value="{{ $seo->page }}">
                    </div>

                    <div class="form-group">
                    <label for="descripcion">Meta Título <small>(Máximo de 70 caracteres)</small></label>
                        <input maxlength="70" type="text" class="form-control" name="titulo" value="{{ $seo->title }}">
                    </div>

                    <div class="form-group">
                    <label for="descripcion">Descripción <small>(Máximo de 155 caracteres)</small></label>
                        <textarea maxlength="155" type="text" class="form-control" name="description">{{ $seo->description }}</textarea>
                    </div>

                    <div class="form-group">
                    <label for="descripcion">Keywords <small>(3 Keywords mínimo)</small></label>
                        <textarea type="text" class="form-control" name="keywords">{{ $seo->keywords }}</textarea>
                    </div>

                    <div class="form-group">
                        <input type="submit" value="Enviar" class="btn btn-primary">
                    </div>
            </form>
        
    </div>
</div>


@endsection