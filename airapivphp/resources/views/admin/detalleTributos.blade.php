@extends('..layouts.admin')


@section('content')

<div class="col-md-10 offset-md-1"> 
    <div class="card mt-5">
        <div class="card-body">
            <table class="table">
            <thead>
                <tr>
                <th scope="col">#</th>
                <th scope="col">Título</th>
                <th scope="col">Opciones</th>
                </tr>
            </thead>
            <tbody>
                @foreach($tributos as $tributo)
                <tr>
                <th scope="row">{{$tributo->id}}</th>
                <td>{{$tributo->titulo}}</td>
                <td><a class="btn btn-warning white"  href="{{ url('tributos/edit/'.$tributo->id) }}"> Edit </a> - <a class="btn btn-danger white" href="{{ url('tributos/delete/'.$tributo->id) }}">Borrar</a></td>
                </tr>
                @endforeach
            </tbody>
            </table>
        </div>
    </div>
</div>
@endsection