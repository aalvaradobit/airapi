@extends('..layouts.admin')


@section('content')

<div class="col-md-10 offset-md-1"> 
    <div class="card mt-5 p-5">
        
            <form action="{{ url('addT') }}" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
                <div class="form-group">
                    <label for="titulo">Titulo</label>
                        <input type="text" class="form-control" name="titulo">
                    </div>
                    <div class="form-group">
                    <label for="descripcion">Descripcion</label>
                    <textarea name="editor1" id="editor1" rows="10" cols="80">
                        
                    </textarea>
                    </div>
                    <div class="form-group">
                        <label for="caratula">Caratula</label>
                        <input type="file" name="caratula" class="form-control">
                    </div>
                    <div class="form-group">
                    <label for="portada">Portada</label>
                    <input type="file" name="portada" class="form-control"></div>
                    <div class="form-group">
                        <input type="submit" value="Enviar" class="btn btn-primary">
                    </div>
            </form>
        
    </div>
</div>

<script>
                // Replace the <textarea id="editor1"> with a CKEditor
                // instance, using default configuration.
                CKEDITOR.replace( 'editor1' );
            </script>
@endsection