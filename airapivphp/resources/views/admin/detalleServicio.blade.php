@extends('..layouts.admin')


@section('content')

<div class="col-md-10 offset-md-1"> 
    <div class="card mt-5">
        <div class="card-body">
            <table class="table">
            <thead>
                <tr>
                <th scope="col">#</th>
                <th scope="col">Titulo</th>
                <th scope="col">Fecha</th>
                <th scope="col">Opciones</th>
                </tr>
            </thead>
            <tbody>
                @foreach($servicios as $servicio)
                <tr>
                <th scope="row">{{$servicio->id}}</th>
                <td>{{$servicio->titulo}}</td>
                <td>{{$servicio->fecha}}</td>
                <td><a class="btn btn-warning white" href="{{ url('servicios/edit/'.$servicio->id) }}"> Edit </a> - <a class="btn btn-danger white" href="{{ url('servicios/delete/'.$servicio->id) }}">Borrar</a></td>
                </tr>
                @endforeach
            </tbody>
            </table>
        </div>
    </div>
</div>
@endsection