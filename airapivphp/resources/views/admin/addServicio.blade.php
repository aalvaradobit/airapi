@extends('..layouts.admin')


@section('content')

<div class="col-md-8 offset-md-2"> 
    <div class="card mt-5 p-5">
        
            <form action="{{ url('/addS') }}" method="POST">
            {{ csrf_field() }}
                <div class="form-group col-md-10 offset-md-1">
                <label for="titulo">Título</label>
                    <input type="text" class="form-control "  name="titulo">
                </div>
                <div class="form-group col-md-10 offset-md-1">
                <label for="descripcion">Descripción Corta</label>
                    <textarea name="descripcionC" id="" cols="30" rows="3" class="form-control "  maxlength="200"></textarea>
                </div>
                <div class="form-group col-md-10 offset-md-1">
                <label for="descripcion">Descripción</label>

                    <textarea name="descripcion" id="descripcion" rows="10" cols="80">
                        
                    </textarea>
                </div>
                <div class="form-group col-md-10 offset-md-1">
                <label for="horario">Horario</label>
                    <input type="text" class="form-control "  name="horario">
                </div>
                <div class="form-group col-md-10 offset-md-1">
                <label for="titular">Titular</label>
                    <input type="text" class="form-control "  name="titular">
                </div>
                <div class="form-group col-md-10 offset-md-1">
                <label for="fecha">Fecha</label>
                    <input type="date" class="form-control col-md-8"  name="fecha">
                </div>

                <div class="form-group col-md-10 offset-md-1">
                        <input type="submit" value="Enviar" class="btn btn-primary">
                </div>
            </form>
        
    </div>
</div>


<script>
    CKEDITOR.replace( 'descripcion' );
</script>

@endsection