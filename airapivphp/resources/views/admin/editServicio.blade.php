@extends('..layouts.admin')


@section('content')

<div class="col-md-10 offset-md-1"> 
    <div class="card mt-5"> 
        <div class="card-body">
           <form action="{{ url('/servicios/edit/'.$servicios->id) }}" method="post">
            {{ csrf_field() }}
                <div class="form-group">
                <label for="titulo">Titulo</label>
                    <input type="text" class="form-control" value="{{$servicios->titulo}}" name="titulo">
                </div>
                <div class="form-group">
                <label for="descripcion">Descripcion Corta</label>
                <textarea name="descripcionC" id="" cols="30" rows="3" class="form-control "  maxlength="400">{{$servicios->descripcionCorta}}</textarea>

                </div>
                <div class="form-group">
                <label for="descripcion">Descripcion</label>
                    <textarea name="descripcion" id="descripcion" rows="10" cols="80">
                        {{$servicios->descripcion}}
                    </textarea>
                </div>
                <div class="form-group">
                <label for="horario">Horario</label>
                    <input type="text" class="form-control" value="{{$servicios->horario}}" name="horario">
                </div>
                <div class="form-group">
                <label for="titular">Titular</label>
                    <input type="text" class="form-control" value="{{$servicios->titular}}" name="titular">
                </div>
                <div class="form-group">
                <label for="fecha">Fecha</label>
                    <input type="date" class="form-control" value="{{$servicios->fecha}}" name="fecha">
                </div>

                <div class="form-group">
                        <input type="submit" value="Enviar" class="btn btn-primary">
                </div>
            </form>
        </div>
        
    </div>
</div>
<script>
    CKEDITOR.replace( 'descripcion' );
</script>
@endsection