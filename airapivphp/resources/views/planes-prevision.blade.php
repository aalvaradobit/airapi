@extends('..layouts.app')

@section('title', 'Ubicación y oficinas de Airapi Memorial Park en Querétaro')
@section('description', 'Airapi Memorial Park, Camino a Acequia Blanca, Querétaro, Qro. Tel. 442 167 1476.')
@section('keywords', 'airapi memorial park, ubicación airapi, oficinas airapi')


@section('content')

<section class="pt-5">
<div class="pt-10">
  <img class="pl-4" src="img/separador-titulo.png" alt="Separador">
  <h2 class="pl-4 pb-3 title Quercus">PLANES DE PREVISIÓN</h2>
    <div class="container1">
        <img class="w-100" src="img/planes/planes_back_beneficios.jpg" alt="">
    </div>
    <div class="planesHeaderText col-md-8 Quercus">
      <img class="" src="img/separador-blanco.png" alt="Separador">
        <h3 class="QuercusBold subPrevision pb-5">BENEFICIOS DE TENER UN <br> PLAN DE PREVISIÓN</h3>
        <ul class="list-group">
          <div class="row noBorder transparent">
            <div class="col-md-6">
                <li class="list-group-item transparent"><img src="img/golden_bullet.png" alt="AirapiBullet"> Tranquilidad y confianza de contar con asesoría y acompañamiento profesional</li>
                <li class="list-group-item transparent"><img src="img/golden_bullet.png" alt="AirapiBullet"> Evitar gastos inesperados y proteger a los tuyos</li>
                <li class="list-group-item transparent"><img src="img/golden_bullet.png" alt="AirapiBullet"> Instalaciones y servicios de primer nivel</li>
            </div>
            <div class="col-md-6">
                <li class="list-group-item transparent"><img src="img/golden_bullet.png" alt="AirapiBullet"> Precio especial por inauguración</li>
                <li class="list-group-item transparent"><img src="img/golden_bullet.png" alt="AirapiBullet"> Facilidades de pago</li>
                <li class="list-group-item transparent"><img src="img/golden_bullet.png" alt="AirapiBullet"> Soluciones integrales e incluyentes </li>
                <li class="list-group-item transparent"><img src="img/golden_bullet.png" alt="AirapiBullet"> Transferible a terceros, para ayudar a tus familiares y amigos que lo necesiten</li>
            </div>
         </div>
        </ul>
    </div>
    </section>

<div class="planesTable">
<img class="pl-4" src="img/separador-titulo.png" alt="Separador">
<h2 class="pl-4 pb-3 title">NUESTROS PLANES</h2>

<table class="table col-md-10 offset-md-1 pb-4 w-100">
  <thead>
    <tr>
      <th scope="col"></th>
      <th scope="col" class="planPrevisionTitle">AMET</th>
      <th scope="col" class="planPrevisionTitle">SENSU</th>
      <th scope="col"  class="noLine planPrevisionTitle">ALTUM</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th scope="row">Tipo de Sala</th>
      <td>A</td>
      <td>AA</td>
      <td class="noLine">AAA</td>
    </tr>
    <tr>
      <th scope="row">Recolección (Dentro del área metropolitana)</th>
      <td>•</td>
      <td>•</td>
      <td class="noLine">•</td>
    </tr>
    <tr>
      <th scope="row">Orientación y gestión de trámites en una sola llamada</th>
      <td>•</td>
      <td>•</td>
      <td class="noLine">•</td>
    </tr>
    <tr>
      <th scope="row">Traslado (Dentro del área metropolitana)</th>
      <td>•</td>
      <td>•</td>
      <td class="noLine">•</td>
    </tr>
    <tr>
      <th scope="row">Sala de velación de lujo (24 hrs)</th>
      <td class="QuercusBold">70 m <sup>2</sup></td>
      <td class="QuercusBold">120 m <sup>2</sup></td>
      <td class="noLine QuercusBold">150 m <sup>2</sup></td>
    </tr>
    <tr>
      <th scope="row">Suite privada (24 hrs)</th>
      <td></td>
      <td>•</td>
      <td class="noLine">•</td>
    </tr>
    <tr>
      <th scope="row">Mesa de tirbuto a tu ser querido</th>
      <td>•</td>
      <td>•</td>
      <td class="noLine">•</td>
    </tr>
    <tr>
      <th scope="row">Ataúd de Madera</th>
      <td>Categoría AMET</td>
      <td>Categoría SENSU</td>
      <td class="noLine">Categoría ALTUM</td>
    </tr>
    <tr>
      <th scope="row">Urna</th>
      <td>Categoría AMET</td>
      <td>Categoría SENSU</td>
      <td class="noLine">Categoría ALTUM</td>
    </tr>
    <tr>
      <th scope="row">Arreglo estético</th>
      <td>•</td>
      <td>•</td>
      <td class="noLine">•</td>
    </tr>
    <tr>
      <th scope="row">Laboratorio de Tanatopraxia</th>
      <td>•</td>
      <td>•</td>
      <td class="noLine">•</td>
    </tr>
    <tr>
      <th scope="row">Servicio de cremación o Inhumación</th>
      <td>•</td>
      <td>•</td>
      <td class="noLine">•</td>
    </tr>
    <tr>
      <th scope="row">Homenaje virtual fotográfico</th>
      <td>•</td>
      <td>•</td>
      <td class="noLine">•</td>
    </tr>
    <tr>
      <th scope="row">Desayuno</th>
      <td>Hasta 5 personas</td>
      <td>Hasta 10 personas</td>
      <td class="noLine">Hasta 15 personas</td>
    </tr>
    <tr>
      <th scope="row">Servicio de catering</th>
      <td>Categoría AMET</td>
      <td>Categoría SENSU</td>
      <td class="noLine">Categoría ALTUM</td>
    </tr>
    <tr>
      <th scope="row">Arreglos Florales</th>
      <td></td>
      <td>2</td>
      <td class="noLine">4</td>
    </tr>
    <tr>
      <th scope="row">Obituario electrónico en página web</th>
      <td>•</td>
      <td>•</td>
      <td class="noLine">•</td>
    </tr>

  </tbody>
</table>

</div>




<div class="planesList pt-5 pb-5">
<img class="pl-5 col-md-3 w-100 pt-3 pb-3" src="img/separador-dorado.png" alt="Separador">
<h3 class="QuercusMedium pl-5">TODOS NUESTROS PLANES INCLUYEN:</h3>
  <div class="row p-4 noBorder no-gutters">
      <div class="col-md-4">
          <ul class="list-group">
              <li class="list-group-item Quercus p-0"><img src="img/golden_bullet.png" alt="AeriBullet"> Ludoteca con niñera </li>
                <img class="col-md-8 pt-3 pb-3" src="img/separador-dorado.png" alt="Separador">
              <li class="list-group-item Quercus p-0"><img src="img/golden_bullet.png" alt="AeriBullet"> Business Center</li>
                <img class="col-md-8 pt-3 pb-3" src="img/separador-dorado.png" alt="Separador">
              <li class="list-group-item Quercus p-0"><img src="img/golden_bullet.png" alt="AeriBullet"> Servicio de mesero </li>
          </ul>
      </div>
      <div class="col-md-4 ">
          <ul class="list-group">
              <li class="list-group-item Quercus p-0"><img src="img/golden_bullet.png" alt="AeriBullet"> Remembranza en árbol del recuerdo </li>
              <img class="col-md-8 pt-3 pb-3" src="img/separador-dorado.png" alt="Separador">
              <li class="list-group-item Quercus p-0"><img src="img/golden_bullet.png" alt="AeriBullet"> Seguridad privada 24hrs </li>
              <img class="col-md-8 pt-3 pb-3" src="img/separador-dorado.png" alt="Separador">
              <li class="list-group-item Quercus p-0"><img src="img/golden_bullet.png" alt="AeriBullet"> Sistema de Monitoreo con CCTV </li>
          </ul>
      </div>
      <div class="col-md-4 ">
          <ul class="list-group">
              <li class="list-group-item Quercus p-0"><img src="img/golden_bullet.png" alt="AeriBullet"> Accesibilidad total a personas con discapacidad </li>
              <img class="col-md-8 pt-3 pb-3" src="img/separador-dorado.png" alt="Separador">
              <li class="list-group-item Quercus p-0"><img src="img/golden_bullet.png" alt="AeriBullet"> Amplio estacionamiento</li>
              <img class="col-md-8 pt-3 pb-3" src="img/separador-dorado.png" alt="Separador">
              <li class="list-group-item Quercus p-0"><img src="img/golden_bullet.png" alt="AeriBullet"> 100% Zona Wifi </li>
          </ul>
      </div>
  </div>
</div>


<div class="planesCard text-center pt-5 pb-5">
    <div class="card noBorder">
        <div class="card-body">
            <div class="col-md-10 offset-md-1">
                <div class="row">
                    <div class="col-md-4 pb-5">
                        <div class="card planCardDetail">
                            <div class="card-body">
                                <h2 class="purple QuercusBold">AMET</h2>

                                <p class="purple QuercusLight">Pago de contado <br> <font class="yellow"> descuento del 25%  </font> </p>
                                <p class="purple QuercusLight">Paga a 12 Meses <br> <font class="yellow"> descuento del 20% </font> </p>
                                <p class="purple QuercusLight">Paga a 24 Meses <br> <font class="yellow"> descuento del 15% </font> </p>

                                <p class="purple QuercusLight">Mensualidad mínima</p>
                                <h2 class="yellow QuercusBold">$1,617.00</h2>
                                <small class="yellow QuercusLight">Precios incluyen I.V.A</small>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 pb-5">
                        <div class="card planCardDetail">
                            <div class="card-body">
                                <h2 class="purple QuercusBold">SENSU</h2>

                                <p class="purple QuercusLight">Pago de contado <br> <font class="yellow"> descuento del 25%  </font> </p>
                                <p class="purple QuercusLight">Paga a 12 Meses <br> <font class="yellow"> descuento del 20% </font> </p>
                                <p class="purple QuercusLight">Paga a 24 Meses <br> <font class="yellow"> descuento del 15% </font> </p>

                                <p class="purple QuercusLight">Mensualidad mínima</p>
                                <h2 class="yellow QuercusBold">$2,706.00</h2>
                                <small class="yellow QuercusLight">Precios incluyen I.V.A</small>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 pb-5">
                        <div class="card planCardDetail">
                            <div class="card-body">
                                <h2 class="purple QuercusBold">ALTUM</h2>

                                <p class="purple QuercusLight">Pago de contado <br> <font class="yellow"> descuento del 25%  </font> </p>
                                <p class="purple QuercusLight">Paga a 12 Meses <br> <font class="yellow"> descuento del 20% </font> </p>
                                <p class="purple QuercusLight">Paga a 24 Meses <br> <font class="yellow"> descuento del 15% </font> </p>

                                <p class="purple QuercusLight">Mensualidad mínima</p>
                                <h2 class="yellow QuercusBold">$3,819.00</h2>
                                <small class="yellow QuercusLight">Precios incluyen I.V.A</small>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="planesBanner text-center p-3 bgYellow3">
    <h2 class="QuercusMedium">HAY COSAS QUE NO SE PUEDEN EVITAR, PERO SI PREVER</h2>
</div>


</div>
@endsection