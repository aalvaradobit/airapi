@extends('..layouts.app')


@section('title', 'Funeraria con velatorio y crematorio en Querétaro, Airapi Memorial Park')
@section('keywords', 'funeraria en querétaro, servicios funerarios en querétaro, velatorios en querétaro, crematorios en querétaro, airapi')
@section('description', 'Servicios funenarrios con los mejores complementos y planes , velatorios, crematorios, recolección y traslado de cuerpo en Querétaro.')


@section('content')

<section class="actividades pt-10">
    <div class="pb-5 pt-5">
    <img class="pl-4" src="img/separador-titulo.png" alt="Separador">
    <h2 class="pl-4 pb-5 title Quercus">PROGRAMA DE ACTIVIDADES</h2>
        <div class="col-md-10 offset-md-1">
                <div class="row">
                    @foreach($actividades as $actividad)
                    <div class="col-md-5 pb-3 col-12">
                            <div class="">
                                <div class="row m-0">
                                    <div class="botCardLeft col-md-9 p-3 col-9 QuercusMedium" style="height:100px;font-size:20px;">
                                        {{$actividad->titulo}}
                                    </div>
                                    <div class="col-md-3 botCardRight col-3 p-3 text-center">
                                    {{ Carbon\Carbon::parse($actividad->fecha)->format('d') }}
                                    <br>
                                    @if( Carbon\Carbon::parse($actividad->fecha)->format('M')  == 'Jan')
                                        Ene
                                    @elseif(  Carbon\Carbon::parse($actividad->fecha)->format('M')  == 'Apr')
                                        Abr
                                    @elseif(  Carbon\Carbon::parse($actividad->fecha)->format('M')  == 'Aug')
                                        Ago
                                    @elseif(  Carbon\Carbon::parse($actividad->fecha)->format('M')  == 'Dec')
                                        Dic
                                    @else
                                        {{ Carbon\Carbon::parse($actividad->fecha)->format('M') }}
                                    @endif
                                    </div>
                                </div>
                                <div class="p-3 pb-5 botCard" style="background-color: #f2f2f2;">
                                    <h5 class="card-title QuercusBold purple">{{$actividad->horario}}</h5>
                                    <p class="card-text QuercusBold purple">Imparte: </p >
                                    
                                    <p class="card-text QuercusBold purple"> Información: </p>
                                    <p class="card-text purple text-justify">{{$actividad->descripcionCorta}}</p>
                                    <p class="card-text text-right info"><a class="yellow" href="{{ url('actividades/'.$actividad->slug) }}"> Más Información </a></p>
                                </div>
                        </div>
                    </div>
                    
                    @endforeach

                    <div class="col-md-5 pb-3 col-12">
                        <div class="">
                            <div class="row m-0">
                                <div class="botCardLeft col-md-9 p-3 col-9 QuercusMedium" style="height:100px;font-size:20px;">
                                    Próximamente
                                </div>
                                <div class="col-md-3 botCardRight col-3 p-3 text-center">
                                        00 <br> -
                                </div>
                            </div>
                            <div class="p-3 pb-5 botCard text-center " style="background-color: #f2f2f2;">
                                <img class="w-50 pt-10" src="{{ url('img/actividades/airapi-actividad-proximamente.png') }}" alt="">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-5 pb-3 col-12">
                        <div class="">
                            <div class="row m-0">
                                <div class="botCardLeft col-md-9 p-3 col-9 QuercusMedium" style="height:100px;font-size:20px;">
                                    Próximamente
                                </div>
                                <div class="col-md-3 botCardRight col-3 p-3 text-center">
                                        00 <br> -
                                </div>
                            </div>
                            <div class="p-3 pb-5 botCard text-center " style="background-color: #f2f2f2;">
                                <img class="w-50 pt-10" src="{{ url('img/actividades/airapi-actividad-proximamente.png') }}" alt="">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-5 pb-3 col-12">
                        <div class="">
                            <div class="row m-0">
                                <div class="botCardLeft col-md-9 p-3 col-9 QuercusMedium" style="height:100px;font-size:20px;">
                                    Próximamente
                                </div>
                                <div class="col-md-3 botCardRight col-3 p-3 text-center">
                                        00 <br> -
                                </div>
                            </div>
                            <div class="p-3 pb-5 botCard text-center " style="background-color: #f2f2f2;">
                                <img class="w-50 pt-10" src="{{ url('img/actividades/airapi-actividad-proximamente.png') }}" alt="">
                            </div>
                        </div>
                    </div>

                </div>
        </div>
    </div>



@endsection