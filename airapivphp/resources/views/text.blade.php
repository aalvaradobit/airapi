<nav class="navbar navbar-expand-lg navbar-light bg-light">
<a href="{{url('/')}}"><img class="logo" src="{{ asset( 'img/airapi-logo.svg') }} " alt="Airapi"></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link purple QuercusMedium" href="{{url('/')}}">Home</a>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle purple QuercusMedium" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        Planes
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="{{url('/planes-inmediato')}}">Uso Inmediato</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="{{url('/planes-prevision')}}">Previsión</a>          
        </div>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="{{url('/servicios')}}">Servicios</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="{{url('/obituarios')}}">Obituarios</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="{{url('/instalaciones')}}">Instalaciones</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="{{url('/tributos')}}">Tributos</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="{{url('/actividad')}}">Actividades</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="{{url('/contacto')}}">Contacto</a>
      </li>
    </ul>
  </div>
</nav>