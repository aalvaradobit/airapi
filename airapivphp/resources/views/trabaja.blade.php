@extends('..layouts.app')


@section('title', 'Funeraria con velatorio y crematorio en Querétaro, Airapi Memorial Park')
@section('keywords', 'funeraria en querétaro, servicios funerarios en querétaro, velatorios en querétaro, crematorios en querétaro, airapi')
@section('description', 'Servicios funenarrios con los mejores complementos y planes , velatorios, crematorios, recolección y traslado de cuerpo en Querétaro.')


@section('content')

<section class="trabaja pt-10">


<img class="pl-4" src="img/separador-titulo.png" alt="Separador">
<h2 class="pl-4 pb-5 title">TRABAJA CON NOSOTROS</h2>

<div class="cardPurpleTrabaja pt-5 text-center">
    <div class="row pt-5 no-gutters">
        <div class="col-md-2">
            <table class="h-100">
            <tbody>
                <tr>
                <td class="align-middle"><img class="w-100" src="img/separador-titulo.png" alt="Separador"></td>
                </tr>
            </tbody>
            </table> 
        </div>
        <div class="col-md-6 offset-md-1">        
            <h3>
         Únete al equipo de trabajo de Airapí
            </h3>
        </div>
        <div class="col-md-2 offset-md-1">
            <table class="h-100">
            <tbody>
                <tr>
                <td class="align-middle"><img class="w-100" src="img/separador-titulo.png" alt="Separador"></td>
                </tr>
            </tbody>
            </table>
        </div>
    </div>
</div>


<div class="form purple col-md-6 offset-md-3 p-5">
    <form action="">
        <div class="form-group">
            <label for="name">Nombre completo: </label>
            <input type="text" class="form-control" name="name">
        </div>
        <div class="form-group">
            <label for="telefono">Punto de interés: </label>
            <input type="text" class="form-control" name="telefono">
        </div>
        <div class="form-group">
            <label for="email">Teléfono:</label>
            <input type="text" class="form-control" name="email">
        </div>
        <div class="form-group">
            <label for="mensaje">Curriculum:</label>
            <input type="file" name="mensaje"class="form-control" id="" value="Sube tu CV aquí">
        </div>
        <input type="submit" value="Enviar" class="btn btn-outline-warning col-md-2 offset-md-5">
    </form>
</div>
</section>


@endsection