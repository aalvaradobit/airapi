@extends('..layouts.app')


@section('title', 'Servicios funerarios en Querétaro, Airapi Memorial Park')
@section('description', 'Funeraria en Querétaro con los mejores servicios funerarios. Brindamos asesoría y tranquilidad para el tributo a su ser querido.')
@section('keywords', 'servicios funerarios en querétaro, memorial park')



@section('content')

<div class="container1 pt-10" >
<img class="w-100" src="{{ url('image/'.$tributos->caratula) }}" alt="{{ $tributos->titulo }}">
</div>
<img class="pl-5 pt-5 pb-3" src="{{ url ('img/separador-dorado.png') }}" alt="Separador">
<h2 class="pl-5 QuercusMedium purple"> {{ $tributos->titulo }}</h2>
<div class="col-md-10 offset-md-1 pt-5 pb-5 tributosText text-justify">
    {!! $tributos->descripcion !!}
 
    
</div>


@endsection