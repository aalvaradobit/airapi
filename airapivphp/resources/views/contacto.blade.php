@extends('..layouts.app')

@section('title', 'Ubicación y oficinas de Airapi Memorial Park en Querétaro')
@section('description', 'Airapi Memorial Park, Camino a Acequia Blanca, Querétaro, Qro. Tel. 442 167 1476.')
@section('keywords', 'airapi memorial park, ubicación airapi, oficinas airapi')


@section('content')


<div class="pt-10">
<img class="pl-4 pt-5" src="img/separador-titulo.png" alt="Separador">
<h2 class="pl-4 pb-3 title Quercus">CONTACTO</h2>

<div class="col-md-12">

<div class="row pb-5">

    <div class="col-md-5 offset-md-1 noBorder">

        <ul class="list-group yellow QuercusLight">
        <li class="list-group-item"><i class="fas fa-globe"></i><a class="purple" href=""> www.airapi.mx</a></li>
        <li class="list-group-item"><i class="fas fa-envelope-open-text"></i><a class="purple" href="mailto:ventas@airaupi.mx"> ventas@airapi.mx</a></li>
        <li class="list-group-item"><i class="fab fa-facebook-f"></i><a class="purple" href="facebook.com/airapimemorialpark"> /airapimemorialpark</a></li>
        <li class="list-group-item"><i class="fas fa-phone-alt"></i><a class="purple" href="telto:442167 1476"> Ventas <br><p class="pl-3"> (442) · 167 · 1476</p></a></li>
        <li class="list-group-item"><i class="fas fa-map-marker-alt"></i><a class="purple" href=""> Camino a Acequia Blanca <br> <p class="pl-3"> (a espaldas de Real de Juriquilla) <br> Juriquilla, Querétaro </p></a></li>
        <li class="list-group-item"><i class="fas fa-map-marker-alt"></i><a class="purple" href="https://www.google.com/maps?ll=20.683864,-100.43452&z=17&t=m&hl=es&gl=US&mapclient=embed&cid=13267135044532333783" target= "_blank"> Oficina de ventas, Local 5 WTC <br> <p class="pl-3">(World Trade Center)</p></a></li>
        <li class="list-group-item"><i class="far fa-clock"></i>  <font class="purple">Lun - Vie <br> <p class="pl-4">8:00 - 18:00</p></font></li>
        </ul>
    </div>

    <div class="col-md-5 ">
        <div class="form purple">
            <form action="">
                    <div class="form-group">
                        <label for="name">Nombre completo: </label>
                        <input type="text" class="form-control" name="name">
                    </div>
                    <div class="form-group">
                        <label for="telefono">Teléfono: </label>
                        <input type="text" class="form-control" name="telefono">
                    </div>
                    <div class="form-group">
                        <label for="email">Correo Electrónico</label>
                        <input type="text" class="form-control" name="email">
                    </div>
                    <div class="form-group">
                        <label for="mensaje">Mensaje</label>
                        <textarea name="mensaje"class="form-control" id="" cols="30" rows="5"></textarea>
                    </div>

                    <input type="submit" value="Enviar" class="btn btn-outline-warning col-md-2 offset-md-5">
                </form>
        </div>
    </div>

    <iframe src="https://www.google.com/maps/embed?pb=!1m16!1m12!1m3!1d59710.5930746272!2d-100.46656405896431!3d20.713795043564325!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!2m1!1sAirapi!5e0!3m2!1ses!2sus!4v1572969988649!5m2!1ses!2sus" width="100%" height="450" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
</div>
</div>

</div>
@endsection