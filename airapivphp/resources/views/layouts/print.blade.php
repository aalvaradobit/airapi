<html lang="es"><head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

    <title>Document</title>

<style>
    .bgModalPurple{
        background-color: rgba(60, 32, 82, 1);
        height:500px;
        padding-bottom: 10%;
    }

    html, body{
        height:100%;
    }

    .w-100{
        width:100%
    }

    body{
        font-style: 'QuercusBook';
    }

    .w-50{
        width:50%
    }

    .text-center{
        text-align:center;
    }
    .white{
        color:white
    }
    .yellow {
        color: #d8a413!important;
    }

    .p-5 {
    padding: 3rem!important;
    }


  

    .p-5{
    padding:5%;
    }
    .m-5{
    margin:5%;
    }

    .float{
        float:left;
    }

    .w-90{
        width:90%;
    }

    .pl-10{
        
        padding-left: 10%;
    }

    p{
        font-size:18px
    }

    h2{
        font-size:35px
    }

    .relative{
        position:relative;
    }

    .absolute{
        position:absolute
    }
</style>



</head><body>

  
        <div id="toPrint" style="width:100%;" class="bgModalPurple">
            <div class="" style="width:100%;">
                <div class="" style="width:20%;float:left;"><img class="w-100" src="{{ url('img/separador-titulo.png') }}" alt="Separador"></div>
                <div class="text-center" style="width:60%;float:left;"><h2 class="white">{{$obituario->nombre}}</h2></div>
                <div class="" style="width:20%;float:left;"><img class="w-100" src="{{ url('img/separador-titulo.png') }}" alt="Separador"></div>

            </div>
        
            <div class="" style="width:100%;">
                    <div class="" style="width:40%;"> 
                        <img class="" src="{{url('obituario/'. $obituario->photoPersona ) }}" alt=""> 
                    </div>
                    <div class="" style="width:100%;">
                        <div class="">
                            <p class="white text-justify">{{$obituario->descripcionEvento}}</p>
                            <p class="white">Agradecemos una oración en su memoria.</p>
                            <p class="white"><strong class="yellow">Tributo: </strong>{{$obituario->ubicacion}}</p>
                            <p class="yellow"> {{$obituario->fechaEvento}}</p>
                            <p class="white">{{$obituario->horarioEvento}}</p>            
                        </div>               
                    </div>
                </div>
            </div>
        </div>
    </body></html>