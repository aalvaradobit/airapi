<!DOCTYPE html>
<html lang="en">
<head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Airapi -  @yield('title')</title>

      <!-- Fonts -->
      <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        <link rel="stylesheet" href="{{ asset( 'css/fonts.css' ) }}  ">
        <script src="{{ asset('ckeditor/ckeditor.js') }}"></script>
        <!-- Styles -->
       
        <link rel="stylesheet" href="{{ asset( 'css/bootstrap.min.css' ) }}">
        <link rel="stylesheet" href="{{ asset( 'css/style.css' ) }}">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <link rel="stylesheet" href="{{ asset( 'css/all.min.css' ) }}">

        <script src="{{ asset('js/all.min.js') }}"></script>
        
        <!-- Bootstrap -->
        <script src="{{ asset('js/jquery.js') }}"></script>
        <script src="{{ asset('js/jqscript.js') }}"></script>
        <script src="{{ asset('js/popper.js') }}"></script>
        <script src="{{ asset('js/bootstrap.min.js') }}"></script>
       
        
    </head>
<body>
    
@section('header')
<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNav">
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" href="{{ url('/') }}"> Ir a Airapi <span class="sr-only">(current)</span></a>
      </li>

      <li class="nav-item">
        <a class="nav-link" href="{{ url('/panel') }}"> Inicio <span class="sr-only">(current)</span></a>
      </li>

      <li class="nav-item">
         <a class="nav-link" href="{{ url('/logout') }}"><i class="fas fa-sign-out-alt"></i>Cerrar Sesion</a>
      </li>
    </ul>
  </div>
</nav>
@show


    @yield('content')

    <div class="alert alert-success alert-dismissible" id="Actualizado" role="alert">
            Actualizado
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>

        <div class="alert alert-danger alert-dismissible" id="Eliminado" role="alert">
            Eliminado
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>

        <div class="alert alert-primary alert-dismissible" id="Agregado" role="alert">
            Agregado
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>

        <style>
            .alert{
                position:fixed;
                right: 20px;
                top: 5%;
                display:none;
            }
        </style>
    
</body>
</html>