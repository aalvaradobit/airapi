<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>@yield('title') |  Airapi</title>

        
        <meta name="description" content="@yield('description')">
        <meta name="keywords" content="@yield('keywords')">
        <meta name="author" content="Bitamina">

        <!-- Metas Geo -->
        <meta name="geo.region" content="MX-QUE" />
        <meta name="geo.placename" content="Santiago de Quer&eacute;taro" />
        <meta name="geo.position" content="20.68384;-100.4345055" />
        <meta name="ICBM" content="20.68384, -100.4345055" />

                <!--Metas OG-->
        <meta property="og:locale" content="es_MX" />
        <meta property="og:title" content="@yield('title') | Airapi" />
        <meta property="og:description" content="@yield('description')" />
        <meta property="og:type" content="website" />
        <meta property="og:url" content="https://www.airapi.com/" />
        <meta property="og:image" content="https://www.airapi.com/img/favicon/favicon.ico" />
        <meta property="og:site_name" content="Airapi" />

            <!--Metas DC-->
    <meta content="@yield('title') | Airapi" NAME='DC.Title'/>
    <meta content="@yield('description')" NAME='DC.Description'/>
    <meta content="Bitamina" NAME='DC.Creator'/>
    <meta content='Airapi' NAME='DC.Publisher'/>
    <meta content="https://www.airapi.com/" NAME='DC.Identifier'/>
    <meta content="@yield('keywords')" NAME='DC.keywords'/>


        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        <link rel="stylesheet" href="{{ asset( 'css/fonts.css' ) }}  ">
        <script src="{{ asset('ckeditor/ckeditor.js') }}"></script>
        <!-- Styles -->
       
        <link rel="stylesheet" href="{{ asset( 'css/bootstrap.min.css' ) }}">
        <link rel="stylesheet" href="{{ asset( 'css/style.css' ) }}">
        
        <link rel="stylesheet" href="{{ asset( 'css/all.min.css' ) }}">

        <script src="{{ asset('js/all.min.js') }}"></script>
        <!-- <script src="{{ asset('js/html2canvas.js') }}"></script>
        <script src="https://unpkg.com/jspdf@latest/dist/jspdf.min.js"></script>
        <script src="{{ asset('js/imagePlugin.js') }}"></script> -->
        
        
        
        <!-- Bootstrap -->
        <script src="{{ asset('js/jquery.js') }}"></script>
        <script src="{{ asset('js/popper.js') }}"></script>
        <script src="{{ asset('js/bootstrap.min.js') }}"></script>
        <script src="{{ asset('js/script.js') }}"></script>
    </head>
    <body>


        @section('header')

        <div class="pos-f-t">
            <header>   
                <div class="headTitle">
                    <a href="tel:4425063445" class="yellow QuercusMedium">Atención inmediata en una sola llamada <br> <font class="white"> (442) 506 • 3445 </font> </a> 
                </div>

                <ul class="redes list-group">
                    <li class="list-group-item"><a target="_blank" href="https://wa.me/5214425063445"><img src="{{ asset( 'img/redes/whatsapp-btn-57x57.svg') }}" alt="WhatsApp"></a></li>
                    <li class="list-group-item"><a target="_blank" href="https://www.facebook.com/pg/AirapiMemorialPark/about/?ref=page_internal"><img src="{{ asset( 'img/redes/facebook-btn-57x57.svg') }}" alt="Facebook"></a></li>
                    <li class="list-group-item"><a target="_blank" href="tel:4421671476"><img src="{{ asset( 'img/redes/llamada-btn-57x57.svg') }}" alt="Telefono"></a></li>
                    <li class="list-group-item"><a target="_blank" href="https://www.google.com/maps?ll=20.683864,-100.43452&z=17&t=m&hl=es&gl=US&mapclient=embed&cid=13267135044532333783"><img src="{{ asset( 'img/redes/googlemaps-btn-57x57.svg') }}" alt="Telefono"></a></li>
                        
                </ul>
            </header>

             <nav class="navbar navbar-expand-lg navbar-light bg-light">
                <a href="{{url('/')}}"><img class="logo" src="{{ asset( 'img/airapi-logo.svg') }} " alt="Airapi"></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav ml-auto">
                    <li class="nav-item pr-4">
                        <a class="nav-link purple QuercusMedium" href="{{url('/')}}">Home</a>
                    </li>
                    <li class="nav-item pr-4 dropdown">
                        <a class="nav-link dropdown-toggle purple QuercusMedium" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Planes
                        </a>
                        <div class="dropdown-menu bgMenu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item purple QuercusMedium" href="{{url('/planes-inmediato')}}">Uso Inmediato</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item purple QuercusMedium" href="{{url('/planes-prevision')}}">Previsión</a>          
                        </div>
                    </li>
                    <li class="nav-item pr-4">
                        <a class="nav-link purple QuercusMedium" href="{{url('/servicios')}}">Servicios</a>
                    </li>
                    <li class="nav-item pr-4">
                        <a class="nav-link purple QuercusMedium" href="{{url('/obituarios')}}">Obituarios</a>
                    </li>
                    <li class="nav-item pr-4">
                        <a class="nav-link purple QuercusMedium" href="{{url('/instalaciones')}}">Instalaciones</a>
                    </li>
                    <li class="nav-item pr-4">
                        <a class="nav-link purple QuercusMedium" href="{{url('/tributos')}}">Tributos</a>
                    </li>
                    <li class="nav-item pr-4">
                        <a class="nav-link purple QuercusMedium" href="{{url('/actividad')}}">Actividades</a>
                    </li>
                    <li class="nav-item pr-4">
                        <a class="nav-link purple QuercusMedium" href="{{url('/contacto')}}">Contacto</a>
                    </li>
                    </ul>
                </div>
            </nav>
        </div>

        
       @show


       <div class="content smallResponsive">

        @yield('content')
       
       </div>
          
       
        @section('footer')
           <footer>
               <div class="">
                   <div class="col-md-10 offset-md-1">
                       <div class="row no-gutters">
                           <div class="col-md-2 col-sm-8 pt-5">
                               <a href="{{ url('/') }}"><img src="{{ asset('img/airapi_footer.png') }}" class="w-100 pt-5 logoFooter" alt="Airapi Contacto"></a>

                           </div>
                           <div class="col-md-1 text-center lineFooter ">
                               <img src="{{ asset('img/vline.png') }}"  alt="vline" >
                           </div>
                           <div class="col-md-3 col-sm-6">
                                  <ul class="list-group pt-5">
                                    <li class="list-group-item pt-3 pl-0 pr-0"><i class="yellow fas fa-map-marker-alt"></i><a class="white" href=""> Camino a Acequia Blanca <br><span class="pl-3" style="font-size: 12.4px;">  (a espaldas de Real de Juriquilla)   </span> <br> <span class="pl-3">  Juriquilla, Querétaro</span> </a></li>
                                    <li class="list-group-item pt-5 pl-0 pr-0"><i class="yellow fas fa-phone-alt"></i><a class="white" href="telto:442167 1476"> Ventas <br> <span class="pl-3"> (442) 167 1476 </span> </a></li>
                                  </ul>
                           </div>

                           <div class="col-md-1 text-center lineFooter">
                                <img src="{{ asset('img/vline.png') }}"  alt="vline" >
                           </div>
                           <div class="col-md-3 col-sm-6">
                         
                                <ul class="list-group white pt-5 text-justify footerMenu">
                                    <li class="list-group-item ">-  <a class="white" href="{{url('/')}}">Home</a></li>
                                    <li class="list-group-item ">-  <a class="white" href="{{url('/planes')}}">Planes</a></li>
                                    <li class="list-group-item ">-  <a class="white" href="{{url('/servicios')}}">Servicios</a></li>
                                    <li class="list-group-item ">-  <a class="white" href="{{url('/obituarios')}}">Obituarios</a></li>                                
                                    <li class="list-group-item ">-  <a class="white" href="{{url('/instalaciones')}}">Instalaciones</a></li>
                                    <li class="list-group-item ">-  <a class="white" href="{{url('/tributos')}}">Tributos</a></li>                                    
                                    <li class="list-group-item ">-  <a class="white" href="{{url('/actividad')}}">Actividades</a></li>    
                                    <li class="list-group-item ">-  <a class="white" href="{{url('/contacto')}}">Contacto</a></li>    
                                </ul>
                           </div>
                           <div class="col-md-2 offset-md-0 col-6  text-center pt-5">
                               <a href="{{ url('/contacto')}}" class="btn btn-secondary contacto QuercusMedium m-3">Contáctanos</a>
                               <a href="{{ url('/contacto')}}" class="btn btn-secondary contactoSimple QuercusMedium m-3">Consulta tu estado de cuenta</a>
                               <a href="{{ url('/trabaja-con-nosotros')}}" class="btn btn-secondary contactoSimple QuercusMedium m-3">Trabaja con nosotros</a>
                            </div>
                       </div>
                       <a class="avisoTxt col-md-8 offset-md-4" href="{{url('/docs/aviso-privacidad.pdf')}}" target="_blank">AVISO DE PRIVACIDAD</a>
                   </div>
               </div>
              <a class="m-2 white" href="{{ url('/auth') }}"><i class="fas fa-lock"></i></a>
           </footer>
        @show
    </body>
</html>




<script>
       
       $('.carousel').carousel({
            interval: 7000
            })
</script>