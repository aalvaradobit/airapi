@extends('..layouts.app')

@section('title', 'Servicios funerarios, crematorios y de velación en Querétaro')
@section('description', 'Los memores planes de previsión y servicios funerarios con crematorio, velación, traslado, cafetería y estacionamiento en Querétaro.')
@section('keywords', 'servicios funerarios en querétaro, crematorios en querétaro, velación en querétaro')

@section('content')

<div class="container1" >
    <img src="{{asset('img/servicios/servicios_mainbanner_01.jpg')}}" alt="Servicios" class="w-100">
    <div class="bannerText2 col-md-5 p-0 col-10">
    <img class="pl-5" src="img/separador-blanco.png" alt="Separador">
    <h2 class="pl-5 QuercusBold letterSpacing">SERVICIOS</h2>
    <p class="pl-5 text-justify">El equipo profesional de Airapí Memorial Park, se encarga de realizar una orientación y gestión integral de todas las necesidades que puedan surgir durante el servicio funerario.</p>

    <p class="pl-5 text-justify">Con un servicio totalmente personalizado y con gran sentido humano para acompañarte en esos momentos vulnerables y de mayor necesidad profesional.</p>  
    </div>
</div>

<div class="cardPurple pt-5 text-center">
    <div class="row pt-5 no-gutters">
        <div class="col-md-2 col-2">
            <table class="h-100">
            <tbody>
                <tr>
                <td class="align-middle"><img class="w-100" src="img/separador-blanco.png" alt="Separador"></td>
                </tr>
            </tbody>
            </table> 
        </div>
        <div class="col-md-6 offset-md-1 col-6 offset-1">        
            <h3>
            PRECIOS ESPECIALES POR INAUGURACIÓN <br> Y DESCUENTOS POR PREVENTA
            </h3>
            <a class=" p-3 mt-3 btn btn-warning white contacto" href="{{ url('/contacto')}}"> Contáctanos </a>
        </div>
        <div class="col-md-2 offset-md-1 col-2 offset-1">
            <table class="h-100">
            <tbody>
                <tr>
                <td class="align-middle"><img class="w-100" src="img/separador-blanco.png" alt="Separador"></td>
                </tr>
            </tbody>
            </table>
        </div>
    </div>
</div>


<div class="bgYellowServicios pt-5">
<img class="pl-5 col-md-3 w-50" src="img/separador-morado.png" alt="Separador">
<h3 class="QuercusMedium pl-5 m-0">SERVICIOS</h3>
</div>
<div class="row p-4 noBorder no-gutters bgYellowServicios pt-3">
    <div class="col-md-4 ">
        <ul class="list-group">
            <li class="list-group-item pb-2 pt-2"><img src="img/purple_bullet.png" alt="AeriBullet"> Orientación y gestión de trámites con una sola llamada</li>
             <p class="m-0"><img class="w-100 col-md-8" src="img/separador-morado.png" alt="Separador"></p>
            <li class="list-group-item pb-2 pt-2"><img src="img/purple_bullet.png" alt="AeriBullet"> Servicio de cremación o inhumación</li>
             <p class="m-0"><img class="w-100 col-md-8" src="img/separador-morado.png" alt="Separador"></p>
            <li class="list-group-item pb-2 pt-2"><img src="img/purple_bullet.png" alt="AeriBullet"> Traslado (dentro de área metropolitana)</li>
             <p class="m-0"><img class="w-100 col-md-8" src="img/separador-morado.png" alt="Separador"></p>
            <li class="list-group-item"><img src="img/purple_bullet.png" alt="AeriBullet"> Arreglo estético</li>
        </ul>
    </div>
    <div class="col-md-4">
        <ul class="list-group">
            <li class="list-group-item pb-2 pt-2"><img src="img/purple_bullet.png" alt="AeriBullet"> Mesa de tributo a tu ser querido</li>
             <p class="m-0"><img class="w-100 col-md-8" src="img/separador-morado.png" alt="Separador"></p>
            <li class="list-group-item pb-2 pt-2"><img src="img/purple_bullet.png" alt="AeriBullet"> Homenaje virtual fotográfico</li>
             <p class="m-0"><img class="w-100 col-md-8" src="img/separador-morado.png" alt="Separador"></p>
            <li class="list-group-item pb-2 pt-2"><img src="img/purple_bullet.png" alt="AeriBullet"> Remembranza en árbol del recuerdo</li>
             <p class="m-0"><img class="w-100 col-md-8" src="img/separador-morado.png" alt="Separador"></p>
            <li class="list-group-item pb-2 pt-2"><img src="img/purple_bullet.png" alt="AeriBullet"> Desayuno para familias</li>
             <p class="m-0"><img class="w-100 col-md-8" src="img/separador-morado.png" alt="Separador"></p>
            <li class="list-group-item"><img src="img/purple_bullet.png" alt="AeriBullet"> Servicio de catering</li>
        </ul>
    </div>
    <div class="col-md-4">
        <ul class="list-group">
            <li class="list-group-item pb-2 pt-2"><img src="img/purple_bullet.png" alt="AeriBullet"> Arreglos florales</li>
             <p class="m-0"><img class="w-100 col-md-8" src="img/separador-morado.png" alt="Separador"></p>
            <li class="list-group-item pb-2 pt-2"><img src="img/purple_bullet.png" alt="AeriBullet"> Obituario electrónico en página web</li>
             <p class="m-0"><img class="w-100 col-md-8" src="img/separador-morado.png" alt="Separador"></p>
            <li class="list-group-item pb-2 pt-2"><img src="img/purple_bullet.png" alt="AeriBullet"> Seguridad privada 24 hrs</li>
             <p class="m-0"><img class="w-100 col-md-8" src="img/separador-morado.png" alt="Separador"></p>
            <li class="list-group-item pb-2 pt-2"><img src="img/purple_bullet.png" alt="AeriBullet"> Sistema de monitoreo con CCTV</li>
             <p class="m-0"><img class="w-100 col-md-8" src="img/separador-morado.png" alt="Separador"></p>
            <li class="list-group-item"><img src="img/purple_bullet.png" alt="AeriBullet"> 100% Zona WiFi</li>
        </ul>
    </div>
</div>



<img class="pl-5 col-md-3 w-100" src="img/separador-titulo.png" alt="Separador">
<h3 class="purple QuercusMedium pl-5">SERVICIOS ADICIONALES</h3>

<div class="row pl-4 pb-4 no-gutters noBorder">
    <div class="col-md-4">
        <ul class="list-group">
            <li class="list-group-item"><img src="img/purple_bullet.png" alt="AeriBullet"> Homenaje póstumo </li>
            <li class="list-group-item"><img src="img/purple_bullet.png" alt="AeriBullet"> Apoyo tanatológico </li>
            <li class="list-group-item"><img src="img/purple_bullet.png" alt="AeriBullet"> Apoyo espiritual </li>
            <li class="list-group-item"><img src="img/purple_bullet.png" alt="AeriBullet"> Celebraciones eucarísticas </li>
            <li class="list-group-item"><img src="img/purple_bullet.png" alt="AeriBullet"> Cursos y talleres abiertos a la comunidad </li>
            <li class="list-group-item"><img src="img/purple_bullet.png" alt="AeriBullet"> Música o coros </li>
            <li class="list-group-item"><img src="img/purple_bullet.png" alt="AeriBullet"> Traslados locales, nacionales e internacionales </li>
            <li class="list-group-item"><img src="img/purple_bullet.png" alt="AeriBullet"> Necesidades especiales que requieran nuestros usuarios </li>
        </ul>
    </div>
    <div class="col-md-8 p-0">
        <img class="w-100" src="img/servicios/servicios-adicionales.jpg"  alt="Servicios Adicionales">
    </div>
</div>



<a class="planesBanner" href="{{ url('/contacto')}}">
<div class="text-center p-5 bgYellow3">
    <h2 class="QuercusBold col-md-8 offset-md-2">¡CONTACTA CON NUESTROS ASESORES!</h2>
    <p class="white col-md-8 offset-md-2">Ellos pueden atender tus solicitudes en todo momento, esperamos tus mensajes con todo gusto.</p>
</div>

</a>


@endsection