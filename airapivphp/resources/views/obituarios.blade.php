@extends('..layouts.app')

@section('title', 'Servicios funerarios, crematorios y de velación en Querétaro')
@section('description', 'Los memores planes de previsión y servicios funerarios con crematorio, velación, traslado, cafetería y estacionamiento en Querétaro.')
@section('keywords', 'servicios funerarios en querétaro, crematorios en querétaro, velación en querétaro')

@section('content')
<script src="http://html2canvas.hertzen.com/dist/html2canvas.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.4.1/jspdf.debug.js"></script>


<section class="obituarios pt-10">
<img class="pl-5 pt-5" src="img/separador-titulo.png" alt="Separador">
<h2 class="pl-5 Quercus purple">OBITUARIOS</h2>

<div class="col-md-10 offset-md-1 p-5 col-lg-10 offset-lg-1">
    <div class="row">

        @foreach($obituarios as $obituario)
          @if($obituario->statusEvento)
            <div class="col-10 offset-1 col-sm-6 offset-sm-0 col-md-6 offset-md-0 col-lg-4 offset-lg-0">
                <div class="card text-center obituarioCard mb-5">
                    <div class="card-body pt-5 pb-5">
                        <h2 class="white">{{$obituario->nombre}}</h2>
                        <h2 class="yellow">{{$obituario->apellidos}}</h2>

                        <p class="white dateObituario">
                            {{ Carbon\Carbon::parse($obituario->fechaEvento)->format('d') }}
                            
                            @if( Carbon\Carbon::parse($obituario->fechaEvento)->format('M')  == 'Jan')
                                Ene
                            @elseif(  Carbon\Carbon::parse($obituario->fechaEvento)->format('M')  == 'Apr')
                                Abr
                            @elseif(  Carbon\Carbon::parse($obituario->fechaEvento)->format('M')  == 'Aug')
                                Ago
                            @elseif(  Carbon\Carbon::parse($obituario->fechaEvento)->format('M')  == 'Dec')
                                Dic
                            @else
                                {{ Carbon\Carbon::parse($obituario->fechaEvento)->format('M') }}
                            @endif 
                            {{ Carbon\Carbon::parse($obituario->fechaEvento)->format('Y') }}
                        </p>
                        <button type="button" class="btn btn-warning white" data-toggle="modal" data-target="#exampleModalCenter{{$obituario->id}}">
                            Detalles
                        </button>
                    </div>
                </div>
            </div>
  <!-- Modal -->
   <div class="modal fade" id="exampleModalCenter{{$obituario->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content bgModalPurple" id="obituari">
                <div class="modal-body">
                    <div id="toPrint" class="bgModalPurple">
                        <div class="row no-gutters ">
                            <div class="col-md-2"><img class="w-100" src="img/separador-titulo.png" alt="Separador"></div>
                            <div class="col-md-8 text-center"><h2 class="white">{{$obituario->nombre}}</h2></div>
                            <div class="col-md-2"><img class="w-100" src="img/separador-titulo.png" alt="Separador"></div>
                        </div>
                    
                        <div class="row p-5">
                            <div class="col-md-4 modalPhoto"> <img class="w-100" src="{{url('obituario/'. $obituario->photoPersona ) }}" alt=""> </div>
                            <div class="col-md-8">
                                <p class="white text-justify">{{$obituario->descripcionEvento}}</p>
                                <p class="white">Agradecemos una oración en su memoria.</p>
                                <p class="white"><strong class="yellow">Tributo: </strong>{{$obituario->ubicacion}}</p>
                                <p class="yellow">     
                            {{ Carbon\Carbon::parse($obituario->fechaEvento)->format('d -') }}
                                
                            @if( Carbon\Carbon::parse($obituario->fechaEvento)->format('M')  == 'Jan')
                                Ene
                            @elseif(  Carbon\Carbon::parse($obituario->fechaEvento)->format('M')  == 'Apr')
                                Abr
                            @elseif(  Carbon\Carbon::parse($obituario->fechaEvento)->format('M')  == 'Aug')
                                Ago
                            @elseif(  Carbon\Carbon::parse($obituario->fechaEvento)->format('M')  == 'Dec')
                                Dic
                            @else
                                {{ Carbon\Carbon::parse($obituario->fechaEvento)->format('M') }}
                            @endif 
                                -
                            {{ Carbon\Carbon::parse($obituario->fechaEvento)->format('Y') }}
                                <p class="white">{{$obituario->horarioEvento}}</p>                           
                            </div>
                        </div>
                    </div>
                       <!-- Default dropup button -->
                    <div class="col-md-8 offset-md-4">
                        <div class="row no-gutters">
                            <div class="col-md-4 offset-md-0 col-6 offset-3 text-center pt-5">
                                <div class="btn-group dropup">
                                    <button type="button" class="btn btn-warning dropdown-toggle white btnObituario" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Como Llegar
                                    </button>
                                    <div class="dropdown-menu obituariosMenu">
                                        <ul class="list-group obituariosRedes">
                                            <a target="_blank" href="https://goo.gl/maps/mGfBfKCKhXWGNsjj8"><li class="list-group-item  pb-1 pt-1"><img src="{{ asset('img/redes/google.svg') }}" alt=""> Google Maps</li></a> 
                                            <p class="m-0"><img class="w-100 col-md-8 offset-md-2" src="img/separador-dorado.png" alt="Separador"></p>
                                            <a target="_blank" href="https://m.uber.com/ul/?action=setPickup&client_id=SziMlthyEIc_orXsBKYSDhNCBphPiFkZ&pickup=my_location&dropoff[latitude]=20.7207289&dropoff[longitude]=-100.4706258"><li class="list-group-item  pt-1 pb-1"><img src="{{ asset('img/redes/uber.svg') }}" alt="">Uber</li></a> 
                                            <p class="m-0"><img class="w-100 col-md-8 offset-md-2" src="img/separador-dorado.png" alt="Separador"></p>
                                            <a target="_blank" href="https://www.waze.com/ul?ll=20.72066488%2C-100.47027111&navigate=yes&zoom=17"><li class="list-group-item  pt-1 pb-1"><img src="{{ asset('img/redes/waze.svg') }}" alt="">Waze</li></a> 
                                        </ul>
                                    </div>
                                </div>
                            </div>                
                            <div class="col-md-5 offset-md-0 col-8 offset-2 text-center pt-5">
                                <button class="btn btn-warning btnObituario white" data-id="{{$obituario->id}}" id="download">Descargar Obituario</button>
                            </div>
                            <div class="col-md-3 offset-md-0 col-6 offset-3">
                                <img class="w-100" src="{{ url('img/obituarios/airapi-logo-obituario.png') }}" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


      
            
            @elseif(!$obituario->statusEvento)
            <div class="col-10 offset-1 col-sm-6 offset-sm-0 col-md-6 offset-md-0 col-lg-4 pb-5 offset-lg-0">
                <div class="card text-center obituarioCardVacia">

                </div>
            </div>
            @endif
        @endforeach
    </div>
</div>
</section>


<!-- 
<script>
    $(document).on('click','#download', function(e){
        e.preventDefault();
        id = $(this).attr('data-id');
        console.log(id);

        $.ajax({
            url: "{{ url('/print') }}"+'/'+id,
            type: "get",
        // data: {
        //     "_token":"{{ csrf_token() }}"
        // },
            
    success: function(result){

            console.log(result);
            }
            
        });
    }); 
</script> -->


<script>

        $('#download').click(function() {
       
            var w = document.getElementById("toPrint").offsetWidth;
            var h = document.getElementById("toPrint").offsetHeight;
            console.log(w);
            console.log(h);
            html2canvas(document.getElementById("toPrint"), {
            dpi: 300, // Set to 300 DPI
            scale: 1, // Adjusts your resolution
            onrendered: function(canvas) {
                var img = canvas.toDataURL("image/JPEG", 1);
                var doc = new jsPDF('L', 'px', [w, h]);
                doc.addImage(img, 'JPEG', 0, 0, w, h);
                doc.save('Obituario.pdf');
            }
            });
        });
        
</script> 


<!-- <script>
$(document).ready(function(){
    $('#download').click(function(){


        html2canvas(document.querySelector("#toPrint")).then(canvas => {
        document.body.appendChild(canvas);
    //         saveAs(canvas.toDataURL(), 'Obituario.png');
           var doc = new jsPDF('p','mm');
            var imgData = canvas.toDataURL('image/JPEG');  
           

            doc.addImage(imgData, 'JPEG', 0, 0);
            doc.save('Obituario.pdf');

        });


        
        function saveAs(uri, filename) {
            var link = document.createElement('a');
            if (typeof link.download === 'string') {
            link.href = uri;
            link.download = filename;

            //Firefox requires the link to be in the body
            document.body.appendChild(link);

            //simulate click
            link.click();

            //remove the link when done
            document.body.removeChild(link);
            } else {
            window.open(uri);
            }
  }


    }); -->
<!-- }); -->
</script> 



@endsection