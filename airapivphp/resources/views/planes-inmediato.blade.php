@extends('..layouts.app')

@section('title', 'Ubicación y oficinas de Airapi Memorial Park en Querétaro')
@section('description', 'Airapi Memorial Park, Camino a Acequia Blanca, Querétaro, Qro. Tel. 442 167 1476.')
@section('keywords', 'airapi memorial park, ubicación airapi, oficinas airapi')


@section('content')

<div class="pt-10">
<img class="pl-4 pt-5" src="img/separador-titulo.png" alt="Separador">
<h2 class="pl-4 pb-3 title Quercus">PLANES DE USO INMEDIATO</h2>

<img class="pl-4" src="img/separador-titulo.png" alt="Separador">
<p class="pl-4 pb-3 purple subTitleInmediato QuercusBold">SERVICIOS A SELECCIONAR</p>

<div class="col-md-10 offset-md-1 p-5">
    <div class="row">
        <div class="col-md-6 pb-5">
            <div class="card planesInmediatoCard">
                <div class="card-body text-center">
                    <p class="purple QuercusMedium">Orientación y gestión de trámites en una sola llamada</p>
                    <img src="img/planes/separador-servicios-bco.png" class="w-100" alt="">
                    <p class="yellow QuercusMedium">Traslado (Dentro del área metropolitana)</p>
                    <img src="img/planes/separador-servicios-bco.png" class="w-100" alt="">
                    <p class="purple QuercusMedium">Sala de velación de lujo de hasta 150m2 (24 hrs)</p>
                    <img src="img/planes/separador-servicios-bco.png" class="w-100" alt="">
                    <p class="yellow QuercusMedium">Suite privada (24 hrs)</p>
                    <img src="img/planes/separador-servicios-bco.png" class="w-100" alt="">
                    <p class="purple QuercusMedium">Mesa de tributo a tu ser querido</p>
                    <img src="img/planes/separador-servicios-bco.png" class="w-100" alt="">
                    <p class="yellow QuercusMedium">Ataúd de Madera</p>
                    <img src="img/planes/separador-servicios-bco.png" class="w-100" alt="">
                    <p class="purple QuercusMedium">Urna</p>
                    <img src="img/planes/separador-servicios-bco.png" class="w-100" alt="">
                    <p class="yellow QuercusMedium">Arreglo estético</p>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="card planesInmediatoCard">
                <div class="card-body text-center">
                    <p class="purple QuercusMedium">Laboratorio de Tanatopraxia</p>
                    <img src="img/planes/separador-servicios-bco.png" class="w-100" alt="">
                    <p class="yellow QuercusMedium">Servicio de cremación o Inhumación</p>
                    <img src="img/planes/separador-servicios-bco.png" class="w-100" alt="">
                    <p class="purple QuercusMedium">Homenaje virtual fotográfico</p>
                    <img src="img/planes/separador-servicios-bco.png" class="w-100" alt="">
                    <p class="yellow QuercusMedium">Desayuno</p>
                    <img src="img/planes/separador-servicios-bco.png" class="w-100" alt="">
                    <p class="purple QuercusMedium">Servicio de catering</p>
                    <img src="img/planes/separador-servicios-bco.png" class="w-100" alt="">
                    <p class="yellow QuercusMedium">Arreglos Florales</p>
                    <img src="img/planes/separador-servicios-bco.png" class="w-100" alt="">
                    <p class="purple QuercusMedium">Obituario electrónico en página web</p>
                </div>
            </div>
        </div>
        </div>
    </div>
</div>

<div class="planesList pb-5 pt-5">
<img class="pl-5 col-md-3 w-100 pt-3 pb-3" src="img/separador-dorado.png" alt="Separador">
<h3 class="Quercus pl-5">TODOS NUESTROS PLANES INCLUYEN:</h3>

    <div class="row p-4 noBorder no-gutters">
        <div class="col-md-4">
            <ul class="list-group ">
                <li class="list-group-item"><img src="img/golden_bullet.png" alt="AeriBullet"> Ludoteca con niñera </li>
                <img class="col-md-8 pt-3 pb-3" src="img/separador-dorado.png" alt="Separador">
                <li class="list-group-item"><img src="img/golden_bullet.png" alt="AeriBullet"> Business Center</li>
                <img class="col-md-8 pt-3 pb-3" src="img/separador-dorado.png" alt="Separador">
                <li class="list-group-item"><img src="img/golden_bullet.png" alt="AeriBullet"> Servicio de mesero </li>
            </ul>
        </div>
        <div class="col-md-4 ">
            <ul class="list-group">
                <li class="list-group-item"><img src="img/golden_bullet.png" alt="AeriBullet"> Remembranza en árbol del recuerdo </li>
                <img class="col-md-8 pt-3 pb-3" src="img/separador-dorado.png" alt="Separador">
                <li class="list-group-item"><img src="img/golden_bullet.png" alt="AeriBullet"> Seguridad privada 24hrs </li>
                <img class="col-md-8 pt-3 pb-3" src="img/separador-dorado.png" alt="Separador">
                <li class="list-group-item"><img src="img/golden_bullet.png" alt="AeriBullet"> Sistema de Monitoreo con CCTV </li>
            </ul>
        </div>
        <div class="col-md-4 ">
            <ul class="list-group">
                <li class="list-group-item"><img src="img/golden_bullet.png" alt="AeriBullet"> Accesibilidad total a personas con discapacidad </li>
                <img class="col-md-8 pt-3 pb-3" src="img/separador-dorado.png" alt="Separador">
                <li class="list-group-item"><img src="img/golden_bullet.png" alt="AeriBullet"> Amplio estacionamiento</li>
                <img class="col-md-8 pt-3 pb-3" src="img/separador-dorado.png" alt="Separador">
                <li class="list-group-item"><img src="img/golden_bullet.png" alt="AeriBullet"> 100% Zona Wifi </li>
            </ul>
        </div>
    </div>
</div>

</div>
<div class="planesBanner text-center p-3 bgYellow3">
    <h2 class="QuercusMedium">NOSOTROS TE APOYAMOS 24 HRS 365 DÍAS AL AÑO</h2>
</div>

@endsection