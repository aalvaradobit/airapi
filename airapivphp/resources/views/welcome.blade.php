@extends('layouts.app')


@section('title', 'Funeraria con velatorio y crematorio en Querétaro, Airapi Memorial Park')
@section('keywords', 'funeraria en querétaro, servicios funerarios en querétaro, velatorios en querétaro, crematorios en querétaro, airapi')
@section('description', 'Servicios funenarrios con los mejores complementos y planes , velatorios, crematorios, recolección y traslado de cuerpo en Querétaro.')


@section('content')

<script>
    $(document).ready(function(){
        // $val = $primer->id;
        // $('#div-'+$val).addClass('active'); 
    });
</script>


<section>
    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
            <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="3"></li>
        </ol>
        <div class="carousel-inner">
                <!-- @foreach($carousel as $car)
                <div class="carousel-item carimg" id="div-{{$car->id}}">
                    <img class="d-block w-100" src="{{url('home/'.$car->photoURL ) }}"  alt="First slide">
                </div>
                @endforeach -->
            <div class="carousel-item active carimg">
                <img class="d-block w-100" src="{{ url ('img/home/home_mainbanner_01.jpg') }}" alt="">
                <p class="textCarrouselHome QuercusBold text-justify col-md-7 col-8">REINVENTAMOS EL SERVICIO TRADICIONAL FUNERARIO EN UNA EXPERIENCIA DE ACOMPAÑAMIENTO Y CALIDAD</p>
            </div>
            <div class="carousel-item carimg">
                <img class="d-block w-100" src="{{ url ('img/home/home_mainbanner_02.jpg') }}" alt="">
                <p class="textCarrouselHome QuercusBold text-justify col-md-7 col-8">CONTAMOS CON PLANES DE PREVISIÓN Y DE USO INMEDIATO</p>
            </div>
            <div class="carousel-item carimg">
                <img class="d-block w-100" src="{{ url ('img/home/home_mainbanner_03.jpg') }}" alt="">
                <p class="textCarrouselHome QuercusBold text-justify col-md-7 col-8">CONTAMOS CON MAUSOLEO CON NICHOS</p>
            </div>
            <div class="carousel-item carimg">
                <img class="d-block w-100" src="{{ url ('img/home/home_mainbanner_04.jpg') }}" alt="">
                <p class="textCarrouselHome QuercusBold text-justify col-md-7 col-8">SOMOS UN CONCEPTO INNOVADOR Y ÚNICO EN QUERÉTARO</p>
            </div>
        </div>
    </div>
</section>

    

    <section class="nosotros">

    <img class="col-md-4 w-100" src="img/separador-titulo.png" alt="Separador">
    <h3 class="white Quercus">Nosotros</h3>
        <div class="col-md-10 pMemorial white Quercus text-justify"> {!! $home->nosotrosTxt   !!}
        </div>        
    </section>

    <a href="{{ url('/planes-prevision') }}" id="planesRedirect">
    <section class="planes pt-5">
    <img class="pl-4" src="img/separador-titulo.png" alt="Separador">
        <h2 class="pl-4 pb-3 title Quercus">PLANES</h2>
        <div class="col-md-10 offset-md-1 col-10 offset-1">
            <div class="row">
                <div class="col-md-4 pb-4">
                    <div class="card cardPlan ">
                        <div class="card-body text-center">
                            <img class="col-md-5" src="img/separador-titulo.png" alt="Separador">
                            <h2 class="m-0 QuercusMedium">AMET</h2>
                            <img class="col-md-5" src="img/separador-titulo.png" alt="Separador">
                            <p>Desde <span class="yellow precioTxt QuercusBold">$1,617.<sup>00</sup> </span><br><small>Mensuales</small></p>                                                    
                        </div>
                    </div>
                </div>
                <div class="col-md-4 pb-4">
                    <div class="card cardPlan">
                        <div class="card-body text-center">
                            <img class="col-md-5" src="img/separador-titulo.png" alt="Separador">
                            <h2 class="m-0 QuercusMedium">SENSU</h2>
                            <img class="col-md-5" src="img/separador-titulo.png" alt="Separador">
                            <p>Desde <span class="yellow precioTxt QuercusBold">$2,706.<sup>00</sup></span><br><small>Mensuales</small></p>                                                    
                        </div>
                    </div>
                </div>
                <div class="col-md-4 pb-4">
                    <div class="card cardPlan">
                        <div class="card-body text-center">
                            <img class="col-md-5" src="img/separador-titulo.png" alt="Separador">
                            <h2 class="m-0 QuercusMedium">ALTUM</h2>
                            <img class="col-md-5" src="img/separador-titulo.png" alt="Separador">
                            <p>Desde <span class="yellow precioTxt QuercusBold ">$3,819.<sup>00</sup></span><br><small>Mensuales</small></p>                                                    
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <p class="bgYellow2 text-center white QuercusMedium p-2"> CONOCE TODOS NUESTROS PLANES </p>
    </section>
    </a>

    <section class="instalaciones pb-5 pt-5">
        <img class="pl-4" src="img/separador-titulo.png" alt="Separador">
        <h2 class="pl-4 pb-3 title Quercus">INSTALACIONES</h2>
            <a href="{{url('/instalaciones')}}">
                <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                <ol class="carousel-indicators">
                    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                </ol>
                    <div class="carousel-inner">
                        <div class="carousel-item active">
                            <div class="row">
                            <div class="col-md-4"><img class="w-100" src="{{ url ('img/instalaciones/instalaciones_galeria_01.jpg') }}" alt=""></div>
                            <div class="col-md-4"><img class="w-100" src="{{ url ('img/instalaciones/instalaciones_galeria_02.jpg') }}" alt=""></div>
                            <div class="col-md-4"><img class="w-100" src="{{ url ('img/instalaciones/instalaciones_galeria_03.jpg') }}" alt=""></div>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <div class="row">
                            <div class="col-md-4"><img class="w-100" src="{{ url ('img/instalaciones/instalaciones_galeria_04.jpg') }}" alt=""></div>
                            <div class="col-md-4"><img class="w-100" src="{{ url ('img/instalaciones/instalaciones_galeria_05.jpg') }}" alt=""></div>
                            <div class="col-md-4"><img class="w-100" src="{{ url ('img/instalaciones/instalaciones_galeria_06.jpg') }}" alt=""></div>
                            </div>
                        </div>
                    </div>        
                </div>
            </a>
    </section>


@endsection


