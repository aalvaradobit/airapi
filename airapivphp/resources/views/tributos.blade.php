@extends('layouts.app')


@section('title', 'Servicios funerarios en Querétaro, Airapi Memorial Park')
@section('description', 'Funeraria en Querétaro con los mejores servicios funerarios. Brindamos asesoría y tranquilidad para el tributo a su ser querido.')
@section('keywords', 'servicios funerarios en querétaro, memorial park')



@section('content')


<div class="section pt-10">
<img class="pl-4 pt-5" src="img/separador-titulo.png" alt="Separador">
<h2 class="pl-4 pb-3 title Quercus">TRIBUTOS</h2>

    @foreach($posts as $post)
    <div class="col-md-10 mb-2 tributosContainer">
        <a href="{{url('/tributos/'. $post->slug  )}}">
        <img class="w-100" src="{{ url('image/'.$post->portada) }}" alt="{{ $post->titulo }}">

        <button class="btnLeer" style="">Leer Más</button>
        </a>
        
    </div>
    @endforeach

    <div class="col-md-10 mb-2 tributosContainer">
       
        <img class="w-100" src="{{ url('img/tributos/cover-tributo-memorial-park.jpg') }}" alt="Memorial Park">
        <button class="btnLeer" style="">Leer Más</button>
       
    </div>
    

</div>

@endsection