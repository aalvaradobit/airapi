<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlanesPrevisionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('planes_previsions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('imgPortada');
            $table->text('descripcion');
            $table->integer('idLista7')->default(7);
            $table->integer('idLista8')->default(8);
            $table->text('title');
            $table->text('description');
            $table->text('keywords');
            $table->string('textoFinal');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('planes_previsions');
    }
}
