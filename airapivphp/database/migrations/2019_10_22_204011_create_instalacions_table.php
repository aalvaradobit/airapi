<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInstalacionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('instalacions', function (Blueprint $table) {
            $table->increments('id');

            $table->text('descripcion');

            $table->integer('idGaleria2')->default(2);
            $table->integer('idGaleria3')->default(3);
            
            $table->integer('idLista3')->default(3);
            $table->integer('idLista4')->default(4);
            
                // SEO
            $table->text('title');
            $table->text('description');
            $table->text('keywords');


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('instalacions');
    }
}
