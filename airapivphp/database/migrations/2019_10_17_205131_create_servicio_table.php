<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServicioTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('servicio', function (Blueprint $table) {
            $table->increments('id');
            $table->text('descripcion');
            $table->text('portadaImg');
            $table->text('portadaTxt');
           
            $table->text('promoTxt');
            $table->integer('idLista5')->default(5);
            $table->integer('idLista6')->default(6);
            $table->text('imgAdicional');

            // SEO
            $table->text('title');
            $table->text('description');
            $table->text('keywords');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('servicio');
    }
}
