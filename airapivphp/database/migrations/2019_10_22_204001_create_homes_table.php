<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHomesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('homes', function (Blueprint $table) {
            $table->increments('id');
            $table->text('descripcion');
            $table->integer('idLista9')->default(9);
            $table->text('nosotrosTxt');
            $table->string('nosotrosBG');

            $table->integer('idGaleria1')->default(1);
    

            // SEO
            $table->text('title');
            $table->text('description');
            $table->text('keywords');


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('homes');
    }
}
