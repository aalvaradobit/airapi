<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateActividadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('actividads', function (Blueprint $table) {
            $table->increments('id');
            $table->string('titulo');
            $table->date('fecha');
            $table->text('descripcion');
            $table->text('descripcionCorta');
            $table->string('horario');
            $table->string('titular');
            $table->string('slug')->unique;
            $table->text('title');
            $table->text('description');
            $table->text('keywords');
            $table->integer('idSeo')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('actividads');
    }
}
