<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlanesInmediatosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('planes_inmediatos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('descripcion');
            $table->integer('idLista1')->default(1);
            $table->integer('idLista2')->default(2);
            $table->text('title');
            $table->text('description');
            $table->text('keywords');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('planes_inmediatos');
    }
}
