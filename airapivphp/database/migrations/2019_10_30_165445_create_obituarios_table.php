<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateObituariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('obituarios', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre');
            $table->string('apellidos');
            $table->date('fechaEvento');
            $table->text('descripcionEvento');
            $table->string('horarioEvento');
            $table->string('photoPersona');
            $table->string('ubicacion');
            $table->boolean('statusEvento')->default(true);
            $table->integer('idSeo')->default(2);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('obituarios');
    }
}
