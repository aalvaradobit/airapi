<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTipoPlanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tipoPlan', function (Blueprint $table) {
            $table->increments('id');
            $table->string('descripcionPlan');
            $table->string('descuento');
            $table->string('descuento12');
            $table->string('descuento24');
            $table->string('costo');

          

            $table->unsignedBigInteger('id_plan')->default(1);
            $table->foreign('id_plan')->references('id')->on('planes_previsions');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tipoPlan');
    }
}
