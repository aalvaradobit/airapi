<?php

use Illuminate\Database\Seeder;

class GaleriaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('galerias')->insert([
            'descripcion' => 'Galeria Home'
        ]);

        DB::table('galerias')->insert([
            'descripcion' => 'Galeria Instalaciones'
        ]);

        DB::table('galerias')->insert([
            'descripcion' => 'Galeria Instalaciones 2'
        ]);
    }
}
