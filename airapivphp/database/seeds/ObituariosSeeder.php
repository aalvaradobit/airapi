<?php

use Illuminate\Database\Seeder;

class ObituariosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('obituarios')->insert([
            'nombre' => 'Ana Cristina ',
            'apellidos' => 'Gonzales Aguado',
            'fechaEvento' => 30/11/2019,
            'descripcionEvento' => 'La familia y amigos, le comunica el sensible fallecimiento de Ana Cristina González Aguado ocurrido en la ciudad de Querétaro el 30 de octubre de 2019.',
            'horarioEvento' => '19:00 hrs',
            'ubicacion' => '',
            'photoPersona' => 'url.pnh',
            'statusEvento' => true
        ]);

        DB::table('obituarios')->insert([
            'nombre' => '',
            'apellidos' => '',
            'fechaEvento' => '',
            'descripcionEvento' => '',
            'horarioEvento' => '',
            'ubicacion' => '',
            'photoPersona' => '',
            'statusEvento' => true
        ]);

        DB::table('obituarios')->insert([
            'nombre' => '',
            'apellidos' => '',
            'fechaEvento' => '',
            'descripcionEvento' => '',
            'horarioEvento' => '',
            'ubicacion' => '',
            'photoPersona' => '',
            'statusEvento' => true
        ]);

        DB::table('obituarios')->insert([
            'nombre' => '',
            'apellidos' => '',
            'fechaEvento' => '',
            'descripcionEvento' => '',
            'horarioEvento' => '',
            'ubicacion' => '',
            'photoPersona' => '',
            'statusEvento' => false
        ]);

        DB::table('obituarios')->insert([
            'nombre' => '',
            'apellidos' => '',
            'fechaEvento' => '',
            'descripcionEvento' => '',
            'horarioEvento' => '',
            'ubicacion' => '',
            'photoPersona' => '',
            'statusEvento' => false
        ]);

        DB::table('obituarios')->insert([
            'nombre' => '',
            'apellidos' => '',
            'fechaEvento' => '',
            'descripcionEvento' => '',
            'horarioEvento' => '',
            'ubicacion' => '',
            'photoPersona' => '',
            'statusEvento' => false
        ]);
    }
}
