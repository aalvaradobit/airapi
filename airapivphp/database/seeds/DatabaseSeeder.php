<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         $this->call(UsersTableSeeder::class);
         $this->call(PostTableSeeder::class);
         $this->call(ActividadesTableSeeder::class);
         $this->call(HomeTableSeeder::class);
         $this->call(InstalacionTableSeeder::class);
         $this->call(ServicioTableSeeder::class);
         $this->call(GaleriaTableSeeder::class);
         $this->call(ListaTableSeeder::class);

         $this->call(ObituariosSeeder::class);
         $this->call(PlanesTableSeeder::class);
         $this->call(TextoTableSeeder::class);
         
    }
}
