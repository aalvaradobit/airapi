<?php

use Illuminate\Database\Seeder;

class ServicioTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('servicio')->insert([
            'descripcion' => 'Servicio',
            'portadaImg' => '',
            'portadaTxt' => 'El equipo profesional de Airapí Memorial Park, se encarga de realizar una orientación y gestión integral de todas las necesidades que puedan surgir durante el servicio funerario.

            Con un servicio totalmente personalizado y con gran sentido humano para acompañarte en esos momentos vulnerables y de mayor necesidad profesional.',
           
            'promoTxt' => 'PRECIO ESPECIAL POR INAUGURACIONES
            DESCUENTO POR PREVENTA',
            'imgAdicional' => '',

            'title' => 'Servicios funerarios, crematorios y de velación en Querétaro',
            'description' => 'Los memores planes de previsión y servicios funerarios con crematorio, velación, traslado, cafetería y estacionamiento en Querétaro.',
            'keywords' => 'servicios funerarios en querétaro, crematorios en querétaro, velación en querétaro' 
        ]);
    }
}
