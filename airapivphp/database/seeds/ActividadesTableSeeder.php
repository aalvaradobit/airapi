<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Str as Str;
class ActividadesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('actividads')->insert([
            'titulo' => 'Torneo de Golf Juriquilla',
            'fecha' => '2019-08-17',
            'descripcion' => '<p>Un gran</p><p>El duelo es la respuesta normal y saludable de una persona ante una p&eacute;rdida. Describe las emociones que se sienten cuando pierde a alguien o algo importante para usted. Las personas hacen el duelo por muchas razones diferentes, que incluyen:</p><ul><li>La muerte de un ser querido, incluidas las mascotas.</li><li>Un divorcio o cambios en una relaci&oacute;n, incluidas amistades.</li><li>Cambios en su salud o la salud de un ser querido.</li><li>La p&eacute;rdida de un trabajo o cambios en la seguridad financiera.</li><li>Los cambios en su estilo de vida, como por ejemplo durante la jubilaci&oacute;n o cuando se muda a un nuevo lugar.</li></ul><p>La p&eacute;rdida que desencadena el duelo no siempre es f&iacute;sica. Puede experimentar el duelo si le diagnostican a usted o un ser querido una enfermedad importante o se enfrenta a una grave enfermedad. Es posible que haga el duelo por los planes futuros que hab&iacute;a hecho o las maneras en las que va a cambiar la vida.</p><p>El duelo es diferente para cada persona. Puede incluir muchos s&iacute;ntomas emocionales y f&iacute;sicos, incluidos:</p><ul><li><strong>Sentimientos:</strong>&nbsp;Ira, ansiedad, culpa, confusi&oacute;n, negaci&oacute;n, depresi&oacute;n, miedo, culpa, irritabilidad, soledad, insensibilidad, alivio, tristeza, shock o anhelo.</li><li><strong>Pensamientos:</strong>&nbsp;Confusi&oacute;n, dificultad para concentrarse, incredulidad, alucinaciones o preocupaci&oacute;n por lo que se perdi&oacute;.</li><li><strong>Sensaciones f&iacute;sicas:</strong>&nbsp;Mareos, ritmo card&iacute;aco acelerado, fatiga, dolores de cabeza, hiperventilaci&oacute;n, n&aacute;useas o malestar estomacal, dificultad para respirar, opresi&oacute;n o pesadez en la garganta o el pecho, o p&eacute;rdida o aumento de peso.</li><li><strong>Comportamientos:</strong>&nbsp;Episodios de llanto, actividad excesiva, irritabilidad o agresividad, p&eacute;rdida de energ&iacute;a, p&eacute;rdida de inter&eacute;s en actividades agradables, inquietud o dificultad para dormir.</li></ul><p>El duelo a veces se describe como un proceso de 5 etapas: negaci&oacute;n, ira, negociaci&oacute;n, depresi&oacute;n y aceptaci&oacute;n.</p><p>Todas estas reacciones ante una p&eacute;rdida son normales. Sin embargo, no todo el mundo que est&aacute; de duelo experimenta todas estas reacciones. Y no todos las experimentan en el mismo orden. Es com&uacute;n volver a atravesar algunas de estas reacciones, las etapas y s&iacute;ntomas m&aacute;s de una vez.</p><p>aciente fallezca.</p>',
            'descripcionCorta' => 'Un gran número de pacientes con enfermedades neurológicas, se enfrentan a la pérdida de capacidades físicas que los hacen depender de otra persona para poder llevar a cabo las actividades de la vida diaria. Esto trae consigo que tanto el paciente, como su familia, se vea en la necesidad de enfrentar diversos duelos durante el transcurso del padecimiento, antes de que el paciente fallezca.',
            'horario' => '15:00 y 19:00 hrs',
            'slug' => Str::slug('Torneo de Golf Juriquilla'),
            'titular' => ''
        ]);

        DB::table('actividads')->insert([
            'titulo' => '',
            'fecha' => '',
            'descripcion' => '',
            'descripcionCorta' => '',
            'horario' => '',
            'slug' => Str::slug(''),
            'titular' => ''
        ]);
        DB::table('actividads')->insert([
            'titulo' => '',
            'fecha' => '',
            'descripcion' => '',
            'descripcionCorta' => '',
            'horario' => '',
            'slug' => Str::slug(''),
            'titular' => ''
        ]);
        DB::table('actividads')->insert([
            'titulo' => '',
            'fecha' => '',
            'descripcion' => '',
            'descripcionCorta' => '',
            'horario' => '',
            'slug' => Str::slug(''),
            'titular' => ''
        ]);
        
    }
}
