<?php

use Illuminate\Database\Seeder;

class ListaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('listas')->insert([
            'descripcion' => 'Lista Planes Inmediato 1'
        ]);

        DB::table('listas')->insert([
            'descripcion' => 'Lista Planes Inmediato 2'
        ]);

        DB::table('listas')->insert([
            'descripcion' => 'Lista Instalaciones 3'
        ]);

        DB::table('listas')->insert([
            'descripcion' => 'Lista Instalaciones 4'
        ]);

        DB::table('listas')->insert([
            'descripcion' => 'Lista Servicio 5'
        ]);

        DB::table('listas')->insert([
            'descripcion' => 'Lista Servicio 6'
        ]);

        DB::table('listas')->insert([
            'descripcion' => 'Lista Planes Preevision 7'
        ]);

        DB::table('listas')->insert([
            'descripcion' => 'Lista Planes Preevision 8'
        ]);

        DB::table('listas')->insert([
            'descripcion' => 'Lista Home 9'
        ]);
    }
}
