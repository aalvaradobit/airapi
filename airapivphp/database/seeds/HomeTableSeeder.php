<?php

use Illuminate\Database\Seeder;

class HomeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('homes')->insert([
            'descripcion' => 'Home',
            'nosotrosTxt' => 'En Airapí Memorial Park transformamos la atención, acompañamiento y calidad de los servicios funerarios mediante el trabajo profesional y dedicado de nuestros colaboradores, promoviendo la dignidad, el respeto y el tributo a la vida, ocupándonos de todo lo necesario para que estés tranquilo, confortado y rodeado por el afecto de quienes te acompañan.',
            'nosotrosBG' => 'background.jpg',  
                  
            'title' => 'Funeraria con velatorio y crematorio en Querétaro, Airapi Memorial Park',
            'description' => 'Servicios funenarrios con los mejores complementos y planes , velatorios, crematorios, recolección y traslado de cuerpo en Querétaro.',
            'keywords' => 'funeraria en querétaro, servicios funerarios en querétaro, velatorios en querétaro, crematorios en querétaro, airapi' 
        ]);
    }
}
