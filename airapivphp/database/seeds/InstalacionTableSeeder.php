<?php

use Illuminate\Database\Seeder;

class InstalacionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('instalacions')->insert([
            'descripcion' => 'Instalaciones',


            'title' => 'Airapi Memorial Park, salas de velación y Capilla en Querétaro',
            'description' => 'En Memorial Park contamos con 4 salas de velación de lujo, crematorio de última generación, capilla, cafetería y 2 suites privadas en Querétaro.',
            'keywords' => 'airapi memorial park, velación en querétaro, crematorio en querétaro' 
        ]);
    }
}
