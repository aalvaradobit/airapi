<?php

use Illuminate\Database\Seeder;

class PlanesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
     
        DB::table('planes_inmediatos')->insert([
            'descripcion' => 'Planes Inmediato',
            'title' => 'Funeraria con velatorio y crematorio en Querétaro, Airapi Memorial Park',
            'description' => 'Servicios funenarrios con los mejores complementos y planes , velatorios, crematorios, recolección y traslado de cuerpo en Querétaro.',
            'keywords' => 'funeraria en querétaro, servicios funerarios en querétaro, velatorios en querétaro, crematorios en querétaro, airapi' 
        ]);

        DB::table('planes_previsions')->insert([
            'descripcion' => 'Planes Prevision',
            'title' => 'Funeraria con velatorio y crematorio en Querétaro, Airapi Memorial Park',
            'description' => 'Servicios funenarrios con los mejores complementos y planes , velatorios, crematorios, recolección y traslado de cuerpo en Querétaro.',
            'keywords' => 'funeraria en querétaro, servicios funerarios en querétaro, velatorios en querétaro, crematorios en querétaro, airapi' ,
            'textoFinal' => 'HAY COSAS QUE NO SE PUEDEN EVITAR, PERO SI PREVER'
        ]);

        DB::table('tipoPlan')->insert([
            'descripcionPlan' => 'AMET',
            'descuento' => '25%',
            'descuento12' => '20%',
            'descuento24' => '15%',
            'costo' => '1,617.00',

        ]);

        DB::table('tipoPlan')->insert([
            'descripcionPlan' => 'AMET',
            'descuento' => '25%',
            'descuento12' => '20%',
            'descuento24' => '15%',
            'costo' => '2,706.00',
        ]);

        DB::table('tipoPlan')->insert([
            'descripcionPlan' => 'AMET',
            'descuento' => '25%',
            'descuento12' => ' 20%',
            'descuento24' => '15%',
            'costo' => '3,819.00',
        ]);

    }
}
