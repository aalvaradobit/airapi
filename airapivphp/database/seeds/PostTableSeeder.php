<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Str as Str;

class PostTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       
            
        DB::table('post')->insert([
            'titulo' => 'Árbol de la vida',
            'descripcion' => 'El Árbol de la Vida es uno de los símbolos característicos de Airapí Memorial Park, ya que alude a la esencia de nuestra propia existencia. Nuestra vida se representa por el nacimiento, al que representan las profundas raíces. La vida que llevamos está reflejada en el tronco que crece en dirección al cielo. Las ramas, en sus múltiples bifurcaciones simbolizan los cruces de camino que nos encontramos en nuestra vida. El Árbol de la Vida es un símbolo universal, atemporal de espiritualidad, sabiduría, bondad y rendición. A lo largo de la historia distintas razas y religiones lo identifican con distintos nombres o formas. Está situado en la parte central de Airapí Memoria Park, representando una escultura de metal en forma de sauce llorón, en el cual, cada hoja tendrá el nombre de cada una de las personas a las que recordamos, rendiremos tributo y daremos vida a su memoria.',
            'slug' => Str::slug('Árbol de la vida'),
            'portada' => '',
            'caratula' => '',
            'title' => '',
            'description' => '',
            'keywords' => ''
        ]);

        DB::table('post')->insert([
            'titulo' => '¿Qué es tributo a la vida?',
            'descripcion' => 'El tiempo pasa muy rápido, siempre nos encontramos con prisa, llevando un estilo de vida muy acelerado entre diferentes actividades como ir al gimnasio, trabajar, llevar a los niños a la escuela, hacer tareas,juntas de trabajo y tantas cosas que nosotros mismos nos imponemos.Pocas son las veces que nos tomamos el tiempo para pensar en nosotros y en los seres queridos, pero en ocasiones es necesario hacer una pausa, detenernos y contemplar la vida… nuestra vida. Muchas veces, esta etapa de reflexión se tiene después de la pérdida de un ser querido, pensamos en el tiempo que hemos desperdiciado haciendo cosas que no nos gustan, nos imaginamos cómo hubiéramos podido aprovecharlo en compañía de esa persona que hoy hace falta, desafortunadamente ya no se puede hacer nada más, solo encontrar una pronta resignación. Es por eso que hoy te hacemos la invitación a rendirle tributo a la vida, en vida.

            ¿Qué es rendir tributo a la vida?
            Rendir tributo a la vida es una celebración constante, es encontrar la felicidad en los detalles más pequeños y sentirte pleno con lo mucho o poco que tienes, es saber que no hay nada más valioso que estar en compañía de tus seres queridos. Rendir tributo a la vida es hacer un homenaje diario a la verdad, a la honestidad contigo mismo, es ver en la naturaleza la obra de Dios y esforzarte por alcanzar tus metas, aún cuando creas que todo está perdido. Sal de tu zona de confort y emprende nuevas aventuras. Un tributo a la vida es un acto de amor... amor a ti mismo y a los demás. No esperes más para disfrutarla y atrévete a hacer todo eso que siempre has querido, porque vida solo tenemos una y es mejor vivirla en plenitud, mientras aún tenemos tiempo.',
            'slug' => Str::slug('¿Qué es tributo a la vida?'),
            'portada' => '',
            'caratula' => '',
            'title' => '',
            'description' => '',
            'keywords' => ''
        ]);

    }
}
