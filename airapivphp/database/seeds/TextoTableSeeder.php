<?php

use Illuminate\Database\Seeder;

class TextoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         // Carrousel Home
         DB::table('textos')->insert([
            'texto' => 'REINVENTAMOS EL SERVICIO TRADICIONAL FUNERARIO EN UNA EXPERIENCIA DE ACOMPAÑAMIENTO Y CALIDAD',
            'idLista' => 9
         ]);
         DB::table('textos')->insert([
            'texto' => 'CONTAMOS CON PLANES DE PREVISIÓN Y DE USO INMEDIATO',
            'idLista' => 9
         ]);
         DB::table('textos')->insert([
            'texto' => 'CONTAMOS CON MAUSOLEO CON NICHOS',
            'idLista' => 9
         ]);
         DB::table('textos')->insert([
            'texto' => 'SOMOS UN CONCEPTO INNOVADOR Y ÚNICO EN QUERÉTARO',
            'idLista' => 9
         ]);
               // Planes de inmediato

         DB::table('textos')->insert([
            'texto' => 'Orientación y gestión de trámites en una sola llamada',
            'idLista' => 1
         ]);
         DB::table('textos')->insert([
            'texto' => 'Traslado (Dentro del área metropolitana)',
            'idLista' => 1
         ]);
         DB::table('textos')->insert([
            'texto' => 'Sala de velación de lujo de hasta 150m2 (24 hrs)',
            'idLista' => 1
         ]);
         DB::table('textos')->insert([
            'texto' => 'Suite privada (24 hrs)',
            'idLista' => 1
         ]);
         DB::table('textos')->insert([
            'texto' => 'Mesa de tributo a tu ser querido',
            'idLista' => 1
         ]);
         DB::table('textos')->insert([
            'texto' => 'Ataúd de Madera',
            'idLista' => 1
         ]);
         DB::table('textos')->insert([
            'texto' => 'Urna',
            'idLista' => 1
         ]);
         DB::table('textos')->insert([
            'texto' => 'Arreglo estético',
            'idLista' => 1
         ]);
         DB::table('textos')->insert([
            'texto' => 'Laboratorio de Tanatopraxia',
            'idLista' => 1
         ]);
         DB::table('textos')->insert([
            'texto' => 'Servicio de cremación o Inhumación',
            'idLista' => 1
         ]);
         DB::table('textos')->insert([
            'texto' => 'Homenaje virtual fotográfico',
            'idLista' => 1
         ]);
         DB::table('textos')->insert([
            'texto' => 'Desayuno',
            'idLista' => 1
         ]);
         DB::table('textos')->insert([
            'texto' => 'Servicio de catering',
            'idLista' => 1
         ]);
         DB::table('textos')->insert([
            'texto' => 'Arreglos Florales',
            'idLista' => 1
         ]);
         DB::table('textos')->insert([
            'texto' => 'Obituario electrónico en página web',
            'idLista' => 1
         ]);


















      
      

        // Carrousel Instalaciones
        DB::table('textos')->insert([
           'texto' => 'Airapí Memorial Park, es un modelo innovador en servicios funerarios, con altos estándares de calidad, instalaciones modernas diseñadas para dar un servicio de excelencia y cálido acompañamiento humano.',
           'idLista' => 3
        ]);
        DB::table('textos')->insert([
            'texto' => 'En Airapí Memorial Park transformamos la atención, acompañamiento, y calidad de los servicios funerarios mediante el trabajo profesional  y dedicado de nuestros colaboradores, promoviendo la dignidad, el respeto y el tributo a la vida, ocupandonos de todo lo necesario para que estés tranquilo, confortado y rodeado por el afecto de quienes te acompañan.',
            'idLista' => 3
         ]);
         DB::table('textos')->insert([
            'texto' => 'Contamos con las mejores instalaciones para ofrecerte un servicio único, innovador, humano y confortable. Te brindamos soluciones rápidas y eficaces en los momentos más difíciles.',
            'idLista' => 3
         ]);
         DB::table('textos')->insert([
            'texto' => 'Quédate con la tranquilidad de contar con el mejor lugar, en los momentos más difíciles. Nuestras instalaciones son únicas e innovadoras para brindar un servicio en conceptos funerarios fuera de lo habitual.',
            'idLista' => 3
         ]);

         // Instalaciones Lista


         DB::table('textos')->insert([
            'texto' => '4 Salas de velación de lujo',
            'idLista' => 4
         ]);
         DB::table('textos')->insert([
            'texto' => '2 con suites privadas',
            'idLista' => 4
         ]);
         DB::table('textos')->insert([
            'texto' => 'Mausoleo con nichos',
            'idLista' => 4
         ]);
         
         DB::table('textos')->insert([
            'texto' => 'Laboratorio de Tanatopraxia',
            'idLista' => 4
         ]);
         DB::table('textos')->insert([
            'texto' => 'Crematorio de última generación',
            'idLista' => 4
         ]);
         DB::table('textos')->insert([
            'texto' => 'Capilla católica con nichos',
            'idLista' => 4
         ]);
         DB::table('textos')->insert([
            'texto' => 'Oratorio ecuménico',
            'idLista' => 4
         ]);
         DB::table('textos')->insert([
            'texto' => 'Ludoteca con niñera',
            'idLista' => 4
         ]);
         DB::table('textos')->insert([
            'texto' => 'Bussiness Center',
            'idLista' => 4
         ]);
         DB::table('textos')->insert([
            'texto' => 'Cafetería',
            'idLista' => 4
         ]);

         DB::table('textos')->insert([
            'texto' => 'Florería',
            'idLista' => 4
         ]);
        DB::table('textos')->insert([
            'texto' => 'Amplio estacionamiento',
            'idLista' => 4
         ]);
         DB::table('textos')->insert([
            'texto' => 'Accesibilidad total a personas con discapacidad',
            'idLista' => 4
         ]);
        
        
        




        
        
        
        
        
        
        
        
        
        
        
        
        
    }
}
