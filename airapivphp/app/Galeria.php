<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Galeria extends Model
{
    protected $table = 'galerias';

    public function photo()
    {
        return $this->hasMany('App\Photo');
    }

    public function home()
    {
        return $this->belongsTo('App\Galeria');
    }

    public function instalacion()
    {
        return $this->belongsTo('App\Instalacion' , 'idGaleria');
    }

    public function instalacion1()
    {
        return $this->belongsTo('App\Instalacion');
    }

}
