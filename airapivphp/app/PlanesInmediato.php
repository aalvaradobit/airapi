<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PlanesInmediato extends Model
{
    protected $table = 'planes_inmediatos';
}
