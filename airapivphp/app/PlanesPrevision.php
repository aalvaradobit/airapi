<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PlanesPrevision extends Model
{

    protected $table = 'planes_previsions';


    public function planes()
    {
        return $this->hasMany('App\TipoPlan');
    }
}
