<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Texto extends Model
{
    protected $table = 'textos';

    public function lista()
{
    return $this->belongsTo('App\Lista', 'idLista');
}
}
