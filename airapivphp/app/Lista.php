<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Lista extends Model
{
    protected $table = 'listas';

    public function texto()
{
    return $this->hasMany('App\Texto');
}

public function instalacion()
{
    return $this->belongsTo('App\Instalacion');
}

public function instalacion1()
{
    return $this->belongsTo('App\Instalacion');
}

    
}
