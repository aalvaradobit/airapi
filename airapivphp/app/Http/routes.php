<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


Route::get('/' , 'HomeController@index');


Route::get('/tributos' , 'PostController@index');
Route::get('/tributos/{slug}' , 'PostController@show');


Route::get('/image/{filename}', 'AdminController@getImage');
Route::get('/home/{filename}', 'HomeController@getImage');
Route::get('/instalacion/{filename}', 'InstalacionController@getImage');
Route::get('/servicio/{filename}', 'ServiciosController@getImage');
Route::get('/obituario/{filename}', 'ObituariosController@getImage');
Route::get('/prevision/{filename}', 'PlanesPrevisionController@getImage');


Route::get('/servicios', function (){
    return view('servicios');
});

Route::get('/actividades/{slug}' , 'ActividadController@show');


Route::get('/contacto', function (){
    return view('contacto');
});




//Route::get('docs/aviso-privacidad.pdf', );


Route::get('/instalaciones' , 'InstalacionController@show');



Route::get('/actividades/{id}', function (){
    return view('blog.actividadesPost');
});

Route::get('/planes-prevision', function (){
    return view('planes-prevision');
});

Route::get('/planes-inmediato', function (){
    return view('planes-inmediato');
});

Route::get('/trabaja-con-nosotros', function (){
    return view('trabaja');
});


Route::get('/obituarios' , 'ObituariosController@index');
Route::get('/print/{id}' , 'ObituariosController@print');



Route::get('/actividad' , 'ActividadController@index');



Route::auth();

Route::get('/foo', function () {
    $exitCode = Artisan::call('cache:clear');
});



// Logeado
Route::group(['middleware' => 'auth'], function () {

    Route::get('/detalleS', 'AdminController@servicios' );
    Route::get('/detallesT', 'AdminController@tributos');
    Route::get('/detalleSeo', 'SeoController@index');


    Route::get('/panel', function(){
        return view('admin.panel');
    });

    Route::get('/addS', function(){
        return view('admin.addServicio');
    });

    Route::get('/addT', function(){
        return view('admin.addTributos');
    });

    Route::get('/addSeo', function(){
        return view('admin.addSeo');
    });

    Route::post('/addS', 'AdminController@postServicio');
    Route::get('/servicios/edit/{id}/' , 'AdminController@editServicio');
    Route::post('/servicios/edit/{id}/' , 'AdminController@updateServicio');
    Route::get('/servicios/delete/{id}/' , 'AdminController@deleteServicio');

    Route::post('/addT', 'AdminController@postTributo');
    Route::get('/tributos/edit/{id}' , 'AdminController@editPost');
    Route::post('/tributos/edit/{id}' , 'AdminController@updatePost');
    Route::get('/tributos/delete/{id}' , 'AdminController@deletePost');

    Route::post('/addSeo', 'SeoController@create');
    Route::get('/seo/edit/{id}' , 'SeoController@edit');
    Route::post('/editSeo/{id}/' , 'SeoController@update');

    
    Route::get('/home' , 'HomeController@show');
    Route::post('/home' , 'HomeController@update');

    Route::post('/photo' , 'PhotoController@store');
    Route::get('/photo/{id}' , 'PhotoController@delete');

    Route::get('/instalacion' , 'InstalacionController@index');
    Route::post('/instalacion' , 'InstalacionController@update');



    Route::post('/texto' , 'TextoController@store');
    Route::get('/texto/{id}' , 'TextoController@delete');



    Route::get('/servicio' , 'ServiciosController@index');
    Route::post('/servicio' , 'ServiciosController@update');
    Route::post('/servicio2' , 'ServiciosController@updatePhoto');

    Route::get('/obituario' , 'ObituariosController@show');
    Route::post('/obituario/{id}' , 'ObituariosController@update');
    Route::post('/obituario/status/{id}' , 'ObituariosController@updateStatus');

    Route::get('/planesPrevision' , 'PlanesPrevisionController@index');
    Route::post('/planesPrevision/Seo' , 'PlanesPrevisionController@updateSeo');
    Route::post('/planesPrevision/Portada' , 'PlanesPrevisionController@updatePortada');
    Route::post('/planesPrevision/{id}' , 'PlanesPrevisionController@updatePlan');
 

    Route::get('/planesInmediato' , 'PlanesInmediatoController@index');
    Route::post('/planesInmediato' , 'PlanesInmediatoController@update');


});

// Sin Logear
Route::group(['middleware' => 'guest'], function () {
    Route::get('/auth', function(){
        return view('auth.login');
    });
    
});