<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str as Str;

use App\Http\Requests;
use App\Post;
use App\Actividad;


class AdminController extends Controller
{
    
    
    public function tributos(){

        $tributos =  Post::paginate(10);
        return view('admin.detalleTributos', compact("tributos",$tributos));

    }
    
    
    public function servicios(){
        $servicios =  Actividad::paginate(10);
        return view('admin.detalleServicio', compact("servicios",$servicios));
    }

    public function postTributo(Request $request){
        $post = new Post();
        $post->titulo = $request->input('titulo');
        $post->descripcion = $request->input('editor1');
        $slug = $request->input('titulo');
        $slug = Str::slug($slug);


        $post->slug = $slug;
        

        if($request->file('portada')){
        $image = $request->file('portada');
        $img_path = time().$image->getClientOriginalName();
        \Storage::disk('tributos')->put($img_path,  \File::get($image));
        
        $post->portada = $img_path;
        }

        if($request->file('caratula')){
            $caratula = $request->file('caratula');
            $img_path_caratula = 'caratula'.time().$caratula->getClientOriginalName();
            \Storage::disk('tributos')->put($img_path,  \File::get($caratula));
            
            $post->caratula = $img_path;
            }
        
           
        $post->save();

        return redirect('panel');
    }
    
    public function getImage($filename){
        $file = \Storage::disk('tributos')->get($filename);
        return Response($file, 200);
    }
    public function editPost($id)
    {
        $tributos = Post::find($id);  
        return view('admin.editTributos',  compact("tributos"));
    }
 
    public function updatePost($id, Request $request)
    {
        $tributos = Post::find($id); 

        $tributos->titulo = $request->input('titulo');
        $tributos->descripcion = $request->input('editor1');
        $slug = $request->input('titulo');
        $slug = Str::slug($slug);
        $tributos->slug;
        

        if($request->file('portada')){

            $img_name = $tributos->portada; 
            \Storage::disk('tributos')->delete($img_name);
            
            $image = $request->file('portada');
            $img_path = time().$image->getClientOriginalName();
            \Storage::disk('tributos')->put($img_path,  \File::get($image));
           
            $tributos->portada = $img_path;
        }

        if($request->file('caratula')){

            $img_name_caratula = $tributos->caratula; 
            \Storage::disk('tributos')->delete($img_name_caratula);
            
            $caratula = $request->file('caratula');
            $img_path_caratula = 'caratula'.time().$caratula->getClientOriginalName();
            \Storage::disk('tributos')->put($img_path_caratula,  \File::get($caratula));
           
            $tributos->caratula = $img_path_caratula;
        }         
        $tributos->save();
        
        return redirect('panel');
    }
 
    public function deletePost($id)
    {
        $tributos = Post::find($id);  
        $img_name = $tributos->portada; 
        $img_name_caratula = $tributos->caratula; 
        \Storage::disk('tributos')->delete($img_name);
        \Storage::disk('tributos')->delete($img_name_caratula);
        $tributos->delete();

       

        

        return redirect('panel');
    }


    //// Servicios


    public function postServicio(Request $request){
        $servicio = new Actividad();

        $servicio->titulo = $request->input('titulo');
        $servicio->descripcionCorta = $request->input('descripcionC');
        $servicio->descripcion = $request->input('descripcion');
        $servicio->horario = $request->input('horario');
        $servicio->titular = $request->input('titular');
        $servicio->fecha = $request->input('fecha');
        $slug = $request->input('titulo');
        $slug = Str::slug($slug);
        $servicio->slug;
           
        $servicio->save();

        return redirect('panel');
    }


    public function editServicio($id)
    {
        $servicios = Actividad::find($id);   
        return view('admin.editServicio',  compact("servicios"));
    }
 
    public function updateServicio($id, Request $request)
    {
        $servicios = Actividad::find($id); 

        $servicios->titulo = $request->input('titulo');
        $servicios->descripcionCorta = $request->input('descripcionC');
        $servicios->descripcion = $request->input('descripcion');
        $servicios->horario = $request->input('horario');
        $servicios->titular = $request->input('titular');
        $servicios->fecha = $request->input('fecha');
        $slug = $request->input('titulo');
        $slug = Str::slug($slug);
        $servicios->slug;
        $servicios->save();
        
        return redirect('panel');
    }
 
    public function deleteServicio($id)
    {
        $servicios = Actividad::find($id);   
        $servicios->delete();

        return redirect('panel');
    }




}
