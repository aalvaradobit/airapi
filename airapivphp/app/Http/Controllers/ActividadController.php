<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Actividad;

class ActividadController extends Controller
{   
    public function index()
    {
        $actividades = Actividad::all();   

        return view('actividad',  compact("actividades"));
    }
    public function show($slug)
    {
       
        $servicio = Actividad::where('slug', '=', $slug)->firstOrFail();
        return view('actividades',  compact("servicio"));
    }
}
