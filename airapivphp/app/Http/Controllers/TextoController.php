<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Texto;

class TextoController extends Controller
{
    public function store(Request $request){
        $texto = new Texto();
        
        $texto->idLista = $request->input('idInstalacion');
        $texto->texto = $request->input('carrouselTxt');


        $texto->save();

        
        return $texto->id;
    }

    public function delete($id){

            $text = Texto::where('id', '=', $id);

            $text->delete();

            return 'success';
    }
}
