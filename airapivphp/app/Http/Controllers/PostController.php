<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Post;

class PostController extends Controller
{
  
   
   public function index()
   {
      $posts =  Post::paginate(10);
      return view('tributos', compact("posts", $posts));

   }


   public function show($slug)
   {
       $tributos = Post::where('slug', '=', $slug)->firstOrFail();

       return view('blog.tributosPost',  compact("tributos"));
   }



}
