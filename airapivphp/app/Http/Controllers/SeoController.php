<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Seo;

class SeoController extends Controller
{

    public function index(){
        $seos = Seo::paginate(5);
        return view('admin.detalleSeo', compact("seos", $seos));
    }

    public function create(Request $request){
        $seo = new Seo();
        $seo->page = $request->input('page');
        $seo->title = $request->input('titulo');
        $seo->description = $request->input('description');
        $seo->keywords = $request->input('keywords');

        $seo->save();
        
        return redirect('panel');
    }

    public function edit($id){
        $seo = Seo::find($id);

    
        return view('admin.editSeo', compact("seo",$seo));
    }

    public function update(Request $request, $id){
        $seo = Seo::find($id);
        $seo->page = $request->input('page');
        $seo->title = $request->input('titulo');
        $seo->description = $request->input('description');
        $seo->keywords = $request->input('keywords');

        $seo->save();
        
        return redirect('panel');
    }
}
