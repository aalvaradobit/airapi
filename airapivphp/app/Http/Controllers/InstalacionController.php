<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Http\Requests;
use App\Instalacion;
use App\Galeria;

class InstalacionController extends Controller
{
    public function index (){

        $instalaciones = Instalacion::find(1);
       //dd($instalaciones);

        $gal1 = $instalaciones->idGaleria2;
        $gal2 = $instalaciones->idGaleria3;
        $li1 =  $instalaciones->idLista3;
        $li2 =  $instalaciones->idLista4;

           

        $carousel1 = DB::table('galerias')
            ->join('photos', 'galerias.id', '=', 'photos.idGaleria')
            ->where('galerias.id', '=', $gal1)
            ->select('galerias.id', 'photoURL', 'photos.id')
            ->get();

            

        $carousel2 = DB::table('galerias')
            ->join('photos', 'galerias.id', '=', 'photos.idGaleria')
            ->where('galerias.id', '=', $gal2)
            ->select('galerias.id', 'photoURL', 'photos.id')
            ->get();

        $lista = DB::table('listas')
            ->join('textos', 'listas.id', '=', 'textos.idLista')
            ->where('listas.id', '=', $li1)
            ->select('listas.id', 'texto', 'textos.id')
            ->get();

         

        $lista2 = DB::table('listas')
            ->join('textos', 'listas.id', '=', 'textos.idLista')
            ->where('listas.id', '=', $li2)
            ->select('listas.id', 'texto', 'textos.id')
            ->get();

            
        return view('pages.instalacion', compact('instalaciones','carousel1', 'carousel2', 'lista', 'lista2'));
    }   

    public function show (){

        $instalaciones = Instalacion::find(1);
       //dd($instalaciones);

        $gal1 = $instalaciones->idGaleria2;
        $gal2 = $instalaciones->idGaleria3;
        $li1 =  $instalaciones->idLista3;
        $li2 =  $instalaciones->idLista4;

           

        $carousel1 = DB::table('galerias')
            ->join('photos', 'galerias.id', '=', 'photos.idGaleria')
            ->where('galerias.id', '=', $gal1)
            ->select('galerias.id', 'photoURL', 'photos.id')
            ->get();

            

        $carousel2 = DB::table('galerias')
            ->join('photos', 'galerias.id', '=', 'photos.idGaleria')
            ->where('galerias.id', '=', $gal2)
            ->select('galerias.id', 'photoURL', 'photos.id')
            ->get();

            $carousel2 = collect($carousel2);

        $lista = DB::table('listas')
            ->join('textos', 'listas.id', '=', 'textos.idLista')
            ->where('listas.id', '=', $li1)
            ->select('listas.id', 'texto', 'textos.id')
            ->get();

         

        $lista2 = DB::table('listas')
            ->join('textos', 'listas.id', '=', 'textos.idLista')
            ->where('listas.id', '=', $li2)
            ->select('listas.id', 'texto', 'textos.id')
            ->get();

            
            $lista2 = collect($lista2);

            
        return view('instalaciones', compact('instalaciones','carousel1', 'carousel2', 'lista', 'lista2'));
    }   


    public function update(Request $request){

        $instalacion = Instalacion::find(1);   
        $instalacion->title = $request ['title'];
        $instalacion->description = $request ['description'];
        $instalacion->keywords = $request ['keywords'];

        $instalacion->save();

        return view('admin.panel');
        
    }

    public function getImage($filename){
        $file = \Storage::disk('instalacion')->get($filename);
        return Response($file, 200);
    }

}
