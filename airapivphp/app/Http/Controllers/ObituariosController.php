<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Obituario;

class ObituariosController extends Controller
{


    public function index(){
        $obituarios = Obituario::all();   

        return view('obituarios', compact('obituarios'));
    }

//     public function print($id){
//         $obituario = Obituario::find($id);   
//       $pdf = \PDF::loadView('layouts.print', compact('obituario'));
       
      

//        return $pdf->stream('Obituario.pdf');
//    // return view('layouts.print', compact('obituario'));
//     }


    public function show(){

        $obituarios = Obituario::all();   
      
        return view('pages.obituarios', compact('obituarios'));
        
    }  

    public function update($id, Request $request){
        $obituario = Obituario::find($id);


        $obituario->nombre = $request->input('nombre');
        $obituario->fechaEvento = $request->input('fechaEvento');
        $obituario->descripcionEvento = $request->input('descripcionEvento');
        $obituario->horarioEvento = $request->input('horarioEvento');
        $obituario->apellidos = $request->input('apellidos');
        $obituario->ubicacion = $request->input('ubicacion');
        
       

        $file = $request->file('photoPersona');

        $oldFile = $request->input('oldPhotoPersona');

        if($oldFile){
            
        \Storage::disk('obituario')->delete($oldFile);

        }
       
        if($file){
        $img_path = time().$file->getClientOriginalName();
        $img_path = str_replace(' ', '', $img_path);
        \Storage::disk('obituario')->put($img_path,  \File::get($file));
        $obituario->photoPersona = $img_path;
        }
  
            // SEO
        //$obituario->description = $request->input('description');
        //$obituario->keywords = $request->input('keywords');
        //$obituario->keywords = $request->input('keywords');
        $obituario->save();
        return $obituario;
    }


        public function getImage($filename){
        $file = \Storage::disk('obituario')->get($filename);
        return Response($file, 200);
    }


    public function updateStatus($id, Request $request){
        $obituario = Obituario::find($id);
          
        $obituario->statusEvento = $request->input('statusEvento');

        $obituario->save();

        return 'success';
    }

    
}
