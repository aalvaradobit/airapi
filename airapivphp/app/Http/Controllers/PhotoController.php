<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;
use App\Http\Requests;
use App\Photo;

class PhotoController extends Controller
{
    public function store(Request $request){

        $imagen = new Photo();

        $idGaleria = $request->input('id');
        $files = $request->file('carrousel');
        
            $val = 'instalacion';
            if($idGaleria == '1'){
                $val = 'home';
            }
        
            foreach ($files as $file) {

            $img_path = time().$file->getClientOriginalName();
            $img_path = str_replace(' ', '', $img_path);
            \Storage::disk($val)->put($img_path,  \File::get($file));
            
            $imagen->photoURL = $img_path;
            $imagen->idGaleria = $idGaleria;

            $imagen->save();

            }
                
        return redirect('/'.$val);

    }

    public function delete($id){

        $pic = Photo::find($id);

        $img_name= $pic->photoURL;
        $id = $pic->id;
        $idGaleria = $pic->idGaleria;

        $val = 'instalacion';
        if($idGaleria == '1'){
            $val = 'home';
        }
    
       
        \Storage::disk($val)->delete($img_name);
        $pic->delete();

        return $id;
    }
}
