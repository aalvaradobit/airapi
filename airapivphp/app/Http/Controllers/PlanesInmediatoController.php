<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\PlanesInmediato;
use Illuminate\Support\Facades\DB;


class PlanesInmediatoController extends Controller
{
    public function index(){

        $planesI = PlanesInmediato::find(1);
        
        $li1 =  $planesI->idLista1;
        $li2 =  $planesI->idLista2;

            $lista = DB::table('listas')
            ->join('textos', 'listas.id', '=', 'textos.idLista')
            ->where('listas.id', '=', $li1)
            ->select('listas.id', 'texto', 'textos.id')
            ->get();

         

        $lista2 = DB::table('listas')
            ->join('textos', 'listas.id', '=', 'textos.idLista')
            ->where('listas.id', '=', $li2)
            ->select('listas.id', 'texto', 'textos.id')
            ->get();


            return view('pages.planesInmediato', compact('lista', 'lista2', 'planesI'));
    }

    public function update(Request $request){
        $planesI = PlanesInmediato::find(1);
        
        $planesI->title = $request->input('title');
        $planesI->description = $request->input('description');
        $planesI->keywords = $request->input('keywords');
      
        $planesI->save();


        return $planesI;
    }
}
