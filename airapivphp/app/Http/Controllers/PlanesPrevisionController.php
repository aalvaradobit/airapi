<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\PlanesPrevision;
use App\Http\Requests;
use App\TipoPlan;

use Illuminate\Support\Facades\DB;

class PlanesPrevisionController extends Controller
{
   public function index(){

      $prevision = PlanesPrevision::find(1);
      $planes = TipoPlan::all();
    

      $li7 =  $prevision->idLista7;
      $li8 =  $prevision->idLista8;

          $lista = DB::table('listas')
          ->join('textos', 'listas.id', '=', 'textos.idLista')
          ->where('listas.id', '=', $li7)
          ->select('listas.id', 'texto', 'textos.id')
          ->get();

       

         $lista2 = DB::table('listas')
          ->join('textos', 'listas.id', '=', 'textos.idLista')
          ->where('listas.id', '=', $li8)
          ->select('listas.id', 'texto', 'textos.id')
          ->get();
    
    return view('pages.planesPrevision', compact('prevision', 'planes', 'lista', 'lista2'));
   }

   public function updatePortada(Request $request){
      $prevision = PlanesPrevision::find(1);
          

         $oldFile = $request->input('oldPhotoPersona');
         if($oldFile){
         \Storage::disk('prevision')->delete($oldFile);
 
         }

          $file = $request->file('imgPortada');
          if($file){
          $img_path = time().$file->getClientOriginalName();
          $img_path = str_replace(' ', '', $img_path);
          \Storage::disk('prevision')->put($img_path,  \File::get($file));
          $prevision->imgPortada = $img_path;
          }


      $prevision->save();


      return $prevision;
  }

  public function updateSeo(Request $request){
      $prevision = PlanesPrevision::find(1);

      $prevision->title = $request->input('title');
      $prevision->description = $request->input('description');
      $prevision->keywords = $request->input('keywords');

      $prevision->save();

      return $prevision;
  }

  public function updatePlan(Request $request, $id){
      $plan = TipoPlan::find($id);

      $plan->descripcionPlan = $request->input('descripcionPlan');
      $plan->descuento = $request->input('descuento');
      $plan->descuento12 = $request->input('descuento12');
      $plan->descuento24 = $request->input('descuento24');
      $plan->costo = $request->input('costo');  
      
      $plan->save();

      return $plan;



  }

   public function getImage($filename){
      $file = \Storage::disk('prevision')->get($filename);
      return Response($file, 200);
  }


}
