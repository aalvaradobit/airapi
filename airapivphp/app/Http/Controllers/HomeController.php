<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Actividad;
use App\Home;

class HomeController extends Controller
{

    public function index()
    {
        $home = Home::find(1);
        $gal = $home->idGaleria1;
        $li =  $home->idLista9;
        


        $carousel = DB::table('galerias')
            ->join('photos', 'galerias.id', '=', 'photos.idGaleria')
            ->where('galerias.id', '=', $gal)
            ->select('galerias.id', 'photoURL', 'photos.id')
            ->orderBy('photos.id', 'asc')
            ->get();

        $lista = DB::table('listas')
            ->join('textos', 'listas.id', '=', 'textos.idLista')
            ->where('listas.id', '=', $li)
            ->select('listas.id', 'texto', 'textos.id')
            ->get();

      
        $primer = DB::table('galerias')
            ->join('photos', 'galerias.id', '=', 'photos.idGaleria')
            ->where('galerias.id', '=', $gal)
            ->select('galerias.id', 'photoURL', 'photos.id')
            ->orderBy('photos.id', 'asc')
            ->first();
        
            $home->nosotrosTxt = str_replace('&nbsp;', ' ', $home->nosotrosTxt);
           

        return view('welcome', compact('home' , 'carousel', 'primer', 'lista'));

    }


    public function show(){

        $home = Home::find(1);
        $gal = $home->idGaleria1;
        $li1 =  $home->idLista9;
       
        
        $carousel = DB::table('galerias')
            ->join('photos', 'galerias.id', '=', 'photos.idGaleria')
            ->where('galerias.id', '=', $gal)
            ->select('galerias.id', 'photoURL', 'photos.id')
            ->get();
        $lista = DB::table('listas')
            ->join('textos', 'listas.id', '=', 'textos.idLista')
            ->where('listas.id', '=', $li1)
            ->select('listas.id', 'texto', 'textos.id')
            ->get();
        
        return view('pages.home', compact('home', 'carousel', 'lista'));



    }  

    public function update(Request $request){
        $home = Home::find(1);


      
        $home->nosotrosTxt = $request->input('nosotrosTxt');

        
        $file = $request->file('nosotrosBG');
        if($file){
        $img_path = time().$file->getClientOriginalName();
        $img_path = str_replace(' ', '', $img_path);
        \Storage::disk('home')->put($img_path,  \File::get($file));
        $home->nosotrosBG = $img_path;
        }


        $home->title = $request->input('title');
        $home->description = $request->input('description');
        $home->keywords = $request->input('keywords');

        $home->save();

        return redirect('/home');
    }

        public function getImage($filename){
        $file = \Storage::disk('home')->get($filename);
        return Response($file, 200);
    }

    
}
