<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Servicio;

class ServiciosController extends Controller
{
 
    public function index (){

        $servicios = Servicio::find(1);
        $li1 =  $servicios->idLista5;
        $li4 =  $servicios->idLista6;


        $lista = DB::table('listas')
            ->join('textos', 'listas.id', '=', 'textos.idLista')
            ->where('listas.id', '=', $li1)
            ->select('listas.id', 'texto', 'textos.id')
            ->get();


        $lista4 = DB::table('listas')
            ->join('textos', 'listas.id', '=', 'textos.idLista')
            ->where('listas.id', '=', $li4)
            ->select('listas.id', 'texto', 'textos.id')
            ->get();
            


            
        return view('pages.servicio', compact('servicios', 'lista','lista4'));
    }  

    public function update(Request $request){
        $servicio = Servicio::find(1);
        

            $file = $request->file('portadaImg');
            if($file){
            $img_path = time().$file->getClientOriginalName();
            $img_path = str_replace(' ', '', $img_path);
            \Storage::disk('servicio')->put($img_path,  \File::get($file));
            $servicio->portadaImg = $img_path;
            }
            

            $file2 = $request->file('imgAdicional');
            if($file2){
            $img_path2 = time().$file2->getClientOriginalName();
            $img_path2 = '2'.$img_path2;
            $img_path2 = str_replace(' ', '', $img_path2);
            \Storage::disk('servicio')->put($img_path2,  \File::get($file));
            $servicio->imgAdicional = $img_path2;
            }
            
            

        $servicio->portadaTxt = $request->input('portadaTxt');

        $servicio->title = $request->input('title');
        $servicio->description = $request->input('description');
        $servicio->keywords = $request->input('keywords');
        $servicio->imgAdicional = $request->input('imgAdicional');

        $servicio->save();

        return redirect('/servicio');
        
    }



    public function updatePhoto(Request $request){
        $servicio = Servicio::find(1);
            

            $file = $request->file('imgAdicional');
            if($file){
            $img_path = time().$file->getClientOriginalName();
            $img_path = str_replace(' ', '', $img_path);
            \Storage::disk('servicio')->put($img_path,  \File::get($file));
            $servicio->imgAdicional = $img_path;
            }
            

        $servicio->save();

        return redirect('/servicio');
        
    }

    public function getImage($filename){
        $file = \Storage::disk('servicio')->get($filename);
        return Response($file, 200);
    }
    

}
