<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoPlan extends Model
{

    protected $table = 'tipoPlan';


    public function tipoPlan()
    {
        return $this->belongsTo('App\PlanesPrevision', 'id_plan');
    }
}

